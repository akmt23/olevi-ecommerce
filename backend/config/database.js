module.exports = ({ env }) => ({
  defaultConnection: "default",
  connections: {
    default: {
      // connector: 'bookshelf',
      // settings: {
      //   client: 'sqlite',
      //   filename: env('DATABASE_FILENAME', '.tmp/data.db'),
      // },
      // options: {
      //   useNullAsDefault: true,
      // },
      connector: "bookshelf",
      settings: {
        client: "postgres",
        // host: env("DATABASE_HOST", "localhost"),
        host: env("DATABASE_HOST", "104.248.139.194"),
        port: env.int("DATABASE_PORT", 5432),
        database: env("DATABASE_NAME", "olevi"),
        username: env("DATABASE_USERNAME", "hellomik"),
        password: env("DATABASE_PASSWORD", "2106"),
        schema: env("DATABASE_SCHEMA", "public"), // Not Required
      },
      options: {},
    },
  },
});

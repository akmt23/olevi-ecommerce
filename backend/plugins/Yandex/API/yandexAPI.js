const { headers } = require('./headers')
const axios  = require('axios').default;
class YandexAPI {
    static async createOrder({
        request_id,
        items,
        routePoints,
        skipDoorToDoor,
        emergency_contact,
        taxi_class,
        callback_properties
    }) {
        try {
            if (!request_id) throw "Request id can not be null";
            if (!items) throw "Pass Items";
            if (!routePoints) throw "Pass Route Points";
            
            const createClaim = {
                items: items,
                route_points: routePoints,
                client_requirements: {
                  taxi_class
                },
                emergency_contact: emergency_contact,
                skip_door_to_door: skipDoorToDoor,
                callback_properties: callback_properties,
              };
            // console.log
            // console.dir(createClaim.route_points)
            
            const responce = await axios.post(
              "https://b2b.taxi.yandex.net/b2b/cargo/integration/v2/claims/create",
              JSON.stringify(createClaim),
              {
                headers: headers,
                params: {
                  request_id: request_id,
                },
              }
            )
          
            
            
            
            
            
            if (responce.status === 400) {
              console.log("shit")
              console.log(responce);
              throw responce.data;
            }
            return responce.data;
        } catch(e) {
            console.log(`${e}`);
            throw e;
        }
    };
    async acceptOrder({ claim_id, version }) {
        try {
            if (!version) throw "Pass Version";
            if (!claim_id) throw "Pass claim_id";
            const responce = await axios.post(
              "https://b2b.taxi.yandex.net/b2b/cargo/integration/v1/claims/accept",
              JSON.stringify({ version:version }),
              {
                headers: headers,
                params: {
                  claim_id: claim_id,
                },
              }
            );
            if (responce.status != 200) {
              console.log("Yandex Accept claim");
              throw responce.data;
            };
            return responce;
        } catch(e) {
            console.log(`${e}`);
            throw e;
        }
    }
    async cancelOrder({ claim_id, version, cancel_state }) {
        try {
            if (!cancel_state) throw "PASS cancel_state";
            if (!version) throw "PASS version";
            const responce = await axios.post(
              "https://b2b.taxi.yandex.net/b2b/cargo/integration/v1/claims/cancel",
              JSON.stringify({
                cancel_state: __.cancel_state,
                version: __.version,
              }),
              {
                headers,
                params: {
                  claim_id: __.claim_id,
                },
              }
            );    
        } catch (e) {
            console.log(`${e}`);
            throw e;
        }
    }

    async getOrderInfo({ claim_id }) {
        try {
            if (!claim_id) throw "Pass claim_id";
            const responce = await axios.post(
              "https://b2b.taxi.yandex.net/b2b/cargo/integration/v2/claims/info",
              JSON.stringify({}),
              {
                headers,
                params: {
                  claim_id: __.claim_id,
                },
              }
            );
        
            if (responce.status != 200) {
              console.log(" Yandex get info");
              throw responce.data;
            };
            return responce;
        } catch (e) {
            console.log(`${e}`);
            throw e;
        }
    }

    async getConfirmationCode({ claim_id }) {
        try {
            if (!__?.claim_id) throw "Pass claim_id";

            const responce = await axios.post(
              "https://b2b.taxi.yandex.net/b2b/cargo/integration/v2/claims/confirmation_code",
              JSON.stringify({
                claim_id: __.claim_id,
              }),
              { headers }
            );
            if (responce.status != 200) {
              throw responce.data + " Yandex get confirmation code";
            }
            return responce;
          
        } catch (e) {
            console.log(`${e}`)
            throw e;
        }
    }
}

module.exports = YandexAPI
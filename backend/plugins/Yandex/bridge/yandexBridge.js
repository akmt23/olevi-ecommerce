const { uid } = require('uid');
const YandexAPI  = require('../API/yandexAPI');


class YandexBridge {
    static async createOrder({
        products,
        skipDoorToDoor,
        user,
        addressForm,
        pickupAddress,
        taxi_class,
        
    }) {
        const contact = {
            name: user.fullName,
            email: user.email,
            phone: user.phoneNumber
        };
        const yandexAddress = {
            contact,
            address: {
                fullname: addressForm.fullname,
                coordinates: [addressForm.longitude, addressForm.latitude],
                city: addressForm.city,
                comment: addressForm.comment ?? undefined,
                country: "Казахстан",
                description: addressForm.description ?? undefined,
                door_code: addressForm.door_code ?? undefined,
                sflat: addressForm.flat ?? undefined,
                porch: addressForm.porch ?? undefined,
                sfloor: addressForm.floor ?? undefined,
                street: addressForm.street ?? undefined
            },
            point_id: 1,
            type: "destination",
            visit_order: 2,
            skip_confirmation: true
        };

        const yandexProducts = products.map((val) => {
            const product = {
                cost_currency: "KZT",
                cost_value: val.product.price.toString(),
                title: val.product.title,
                quantity: val.capacity,
                droppof_point: 1,
                pickup_point: 133,
                size: {
                    height: val.product.height,
                    width: val.product.width,
                    length: val.product.Length
                }
            }
            return product
        })
        const pickupLocation = {
            address: {
                coordinates: [pickupAddress.longitude, pickupAddress.latitude],
                fullname: pickupAddress.fullname,
                city: pickupAddress.city,
            },
            contact: {
                name: "Olevi",
                phone: "+77755158254",
            },
            point_id: 133,
            type: "source",
            visit_order: 1,
            skip_confirmation: true
        };
        const emergency_contact = {
            name: user.fullName,
            phone: user.phoneNumber
        }
        const request_id = uid(13)
        // console.log(request_id);
        const callback_properties = {
            callback_url: "https://www.qubeme.com",
        }
        // console.dir(callback_properties)
        const routePoints = [yandexAddress, pickupLocation]
        const yandexClaim = await YandexAPI.createOrder({
            items: yandexProducts,
            routePoints,
            request_id,
            skipDoorToDoor,
            taxi_class,
            user,
            callback_properties,
            emergency_contact
            
        });
        return yandexClaim;
    };
    static async accept({claim_id, version}) {
        const yandex = new YandexAPI();
        const accept = yandex.acceptOrder({ claim_id, version });
        return accept;
    }
}

module.exports = { YandexBridge };
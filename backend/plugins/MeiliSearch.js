const { MeiliSearch } = require('meilisearch')
// import { meiliesearch } from "../config/plugins";
const { meilisearch } = require('../config/plugins')


const client = new MeiliSearch(meilisearch)

class MeiliSearchInteraction {
    index
    constructor(indexName) {
        this.index = client.index(indexName);
    }

    async insertDocuments(documents) {
        const responce = await this.index.addDocuments(documents)
        return responce;
    };

    async searchDocuments(searchText) {
        const searchResult = await this.index.search(`${searchText}`);
        return searchResult;
    }
}

module.exports = {client, MeiliSearchInteraction};
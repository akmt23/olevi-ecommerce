'use strict';
const { YandexBridge } = require('../../../plugins/Yandex/bridge/yandexBridge');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */



module.exports = {
   /**
     * Retrieve a record.
     *
     * @return {Object}
     */

    // product
    // capacity
    async create(ctx) {
        const args = ctx.request.body;
        const user = {
            ...args.user
        };
        console.log("START")
        const findUnsortedProducts = await Promise.all(
            args.products.map(async (val) => {
                const product = await strapi.query('product').findOne({ id: val.id });
                return product;
            })
        );
        
        const productIDs = findUnsortedProducts.reduce((map, findUnsortedProducts) => {
            return {
                ...map,
                [findUnsortedProducts.id]: findUnsortedProducts
            }
        }, {});
        const sortedProducts = args.products.map((val) => {
            return {
                product: productIDs[val.id],
                capacity: val.capacity,
            }
        });
        const yandexOrder = await YandexBridge.createOrder({
            user,
            products: sortedProducts,
            skipDoorToDoor: false,
            addressForm: args.addressForm,
            pickupAddress: args.pickupAddress,
            taxi_class: "express"
        }); 
        
        const strapiAddress = {
            addressName: yandexOrder.route_points[0].address.fullname,
            city: yandexOrder.route_points[0].address.city,
            coordinates: yandexOrder.route_points[0].address.coordinates
        }
        const strapiOrder = {
            claim_id: yandexOrder.id,
            products: args.products.map((val) => val.id),
            address_droppof: strapiAddress
        } 
        // console.log(yandexOrder.id)
        // console.log(yandexOrder.items)
        // console.log(yandexOrder.route_points[0].address)

        await strapi.query('order').create(strapiOrder);
        
        return { yandexOrder };
    }
};

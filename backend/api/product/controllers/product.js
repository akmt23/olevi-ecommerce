const { sanitizeEntity, parseMultipartData } = require('strapi-utils');
const { MeiliSearchInteraction } = require('../../../plugins/MeiliSearch')

const meilieClient = new MeiliSearchInteraction('product');

module.exports = {
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */

  async findOne(ctx) {
    const { id } = ctx.params;
    
    const entity = await strapi.services.product.findOne({ id }, [
      'image',
      'images',
      'feedbacks',
      'feedbacks.users_permissions_user',
      'feedbacks.users_permissions_user.media',
    ]);

    return sanitizeEntity(entity, { model: strapi.models.product });
  },
  async findSimilar(ctx) {
    const { id } = ctx.params;
    const entity = await strapi.services.product.findOne({ id }, ['categories']);
    console.log(entity);
    const categoriesIds = entity.categories.map(e => e.id);
    const _check = await strapi.query('product').find({ categories: categoriesIds, _limit: 40 });
    return sanitizeEntity(_check, { model: strapi.models.product });
  },
  async findByCategry(ctx) {
    const { id } = ctx.params;
    const _check = await strapi.query('product').find({ categories: id, _limit: 40 });
    return sanitizeEntity(_check, { model: strapi.models.product });
  },
  async find(ctx) {
    const filterDataByLocale = (data, locale) => data.filter(el=> el.locale === locale);   
    let entities;
    const locale = ctx.query._locale;
    if (ctx.query._q) {
      entities = await meilieClient.searchDocuments(`${ctx.query._q}`);
    } else {
      entities = await strapi.query('product').find();
      entities = filterDataByLocale(entities, locale);
    }
    return entities;
  }

};


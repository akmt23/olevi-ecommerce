const { sanitizeEntity } = require('strapi-utils');
const { MeiliSearchInteraction } = require('../../../plugins/MeiliSearch')
const meilieClient = new MeiliSearchInteraction('category');

module.exports = {
  /**
   * Retrieve a record.
   *
   * @return {Object}
   */

  async findOne(ctx) {
    const { slug } = ctx.params;

    const entity = await strapi.services.category.findOne({ slug });
    return sanitizeEntity(entity, { model: strapi.models.category });
  },
  async find(ctx) {
    
    const filterDataByLocale = (data, locale) => data.filter(el=> el.locale === locale);   
    let entities;
    const locale = ctx.query._locale;
    if (ctx.query._q) {
      entities = await meilieClient.searchDocuments(`${ctx.query._q}`);
    } else {
      entities = await strapi.query('category').find();
      entities = filterDataByLocale(entities, locale);
    }
    return entities;
  }
};

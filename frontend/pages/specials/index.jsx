import React from 'react';
import Layout from 'components/templates/Layout';
import ProductCard from '@/atoms/ProductCards';
import { useRouter } from 'next/router';
import { useTranslations } from 'next-intl';

const Special = ({ products }) => {
  const router = useRouter();
  const t = useTranslations('SpecialsPage');

  return (
    <Layout>
      <div className="container">
        <h1 className="font-bold text-2xl my-6">{t('title')}</h1>
        <main className="mb-8 grid grid-cols-4 gap-4">
          <section className="col-start-1 col-end-5 grid grid-cols-2 lg:grid-cols-4 gap-4">
            {products.map(product => {
              if (router.query.id) {
                const isContainedInCategories = product?.categories?.filter(category => category.id == router.query.id).length > 0;
                if (isContainedInCategories) {
                  return <ProductCard product={product}></ProductCard>;
                }
              } else {
                return <ProductCard product={product}></ProductCard>;
              }
            })}
          </section>
        </main>
      </div>
    </Layout>
  );
};

export default Special;

export async function getServerSideProps({ locale }) {
  const productsRes = await fetch(`http://104.248.139.194:1337/products?_locale=${locale}`);
  const products = await productsRes.json();

  return {
    props: {
      products,
      messages: {
        ...require(`../../languages/${locale}.json`),
      },
    }, // will be passed to the page component as props
  };
}

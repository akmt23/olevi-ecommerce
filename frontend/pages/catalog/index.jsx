import React, { useEffect, useMemo, useState } from 'react';
import styled from 'styled-components';
import { useRouter } from 'next/router';
import ProductCard from '@/atoms/ProductCards';
import { motion, AnimatePresence, AnimateSharedLayout } from 'framer-motion';
import Tappable from '@/atoms/Tappable';
import Layout from 'components/templates/Layout';
import ReactPaginate from 'react-paginate';
import InViewPopIn from '@/atoms/InViewPopIn';
import NewProductsForCatalog from '@/organisms/NewProductsForCatalog';
import PriceRangeSlider from '@/atoms/PriceRangeSlider';
import { useTranslations } from 'next-intl';
import useDeviceDetect from '@/hooks/useDeviceDetect';

const CatalogPage = ({ products, hitProducts, categories, messages }) => {
  const router = useRouter();
  const categoryQueryId = parseInt(router.query.id);
  const [checked, setChecked] = useState([]);
  const [pageNumber, setPageNumber] = useState(0);
  const [hitsPageNumber, setHitsPageNumber] = useState(0);
  const [priceRange, setPriceRange] = useState({});
  const [showCategories, setShowCategories] = useState(false);
  const [isFilterReseted, setIsFilterReseted] = useState(false);
  const { isMobile } = useDeviceDetect();
  const t = useTranslations('CatalogPage');
  let priceRangeFromChild = {};

  useEffect(() => {
    const routerCheckedCategoryId = categoryQueryId ? [categoryQueryId] : [];
    setChecked(routerCheckedCategoryId);
  }, [categoryQueryId]);

  const filteredProducts = products.filter(item => {
    return isInCategory(item) & isInPriceRange(item);
  });
  //products with categories
  const productsPerPage = 6;
  const pagesVisited = pageNumber * productsPerPage;
  //hit products
  const hitProductsPerPage = 8;
  const hitPagesVisited = hitsPageNumber * hitProductsPerPage;

  //show products with categories - pagination
  const displayProductsWithPagination = filteredProducts.slice(pagesVisited, pagesVisited + productsPerPage).map(product => {
    return <ProductCard product={product}></ProductCard>;
  });

  //show hit products - pagination
  const displayHitProducts = hitProducts.slice(hitPagesVisited, hitPagesVisited + hitProductsPerPage).map(product => {
    return <ProductCard type="hit" product={product} />;
  });

  function isInCategory(item) {
    if (checked.length === 0) {
      return true;
    }
    const foundCategories = (item.categories || []).filter(value => (checked || []).includes(value.id));
    return foundCategories.length;
  }

  function isInPriceRange(item) {
    if (!item.price) {
      return true;
    }
    if (priceRange.min > -1 && item.price < priceRange.min) {
      return false;
    }
    if (priceRange.max > -1 && item.price > priceRange.max) {
      return false;
    }
    return true;
  }

  const ifCellChecked = sub => {
    return !!checked.find(el => el === sub.id);
  };

  // pagination settings for products with categories
  const pageCount = Math.ceil(filteredProducts.length / productsPerPage);

  const changePage = ({ selected }) => {
    setPageNumber(selected);
  };

  // pagination settings for hit products
  const hitPageCount = Math.ceil(filteredProducts.length / hitProductsPerPage);

  const hitChangePage = ({ selected }) => {
    setHitsPageNumber(selected);
  };

  const handleToggle = index => {
    const currentIndex = checked.indexOf(index);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(index);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
    setShowCategories(!showCategories);
  };

  const handleResetFilter = () => {
    setPriceRange({});
    setChecked([]);
    setShowCategories(!showCategories);
    setIsFilterReseted(!isFilterReseted);
  };

  const isFilterOpen = useMemo(() => {
    if (!isMobile) return true;

    return showCategories;
  }, [showCategories, isMobile]);

  return (
    <Layout>
      <div className="container">
        <div className="flex">
          <h1 className="flex-1 font-bold text-2xl my-6">{t('greeting')}</h1>
          <Tappable className="flex-1 flex items-center justify-end">
            <button onClick={() => setShowCategories(!showCategories)} className="flex items-center justify-end lg:hidden">
              Фильтры
              <span>
                <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
                </svg>
              </span>
            </button>
          </Tappable>
        </div>
        <main className="mb-8 flex flex-col lg:flex-row gap-4">
          <CategoriesContainer show={isFilterOpen} className="w-full lg:min-w-2/6 lg:max-w-2/6 lg:w-2/6 bg-frozen-blue h-full rounded-md p-6">
            <div className="flex justify-between items-center text-deep-blue border-b-2 border-white py-1">
              <span className="font-bold text-lg uppercase">{t('categories')}</span>
              <button>
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                  <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
                </svg>
              </button>
            </div>
            <div className="py-4 h-full flex-col flex justify-start space-y-1">
              {categories?.map(category => (
                <div className="py-1">
                  <Collapsible title={category.name} subActive={category.sub_categories.length ? true : false}>
                    <div className="flex flex-col items-start space-y-2 pl-6 pr-4 mt-2">
                      <div className="bg-white rounded-md p-4 py-4 w-full">
                        {category.sub_categories.map(sub => (
                          <div className="flex items-center">
                            <label className="w-full cursor-pointer">
                              <input type="checkbox" className="mr-2" checked={ifCellChecked(sub)} onChange={() => handleToggle(sub.id)} />
                              <span className="uppercase font-semibold text-sm">{sub.name}</span>
                            </label>
                          </div>
                        ))}
                      </div>
                    </div>
                  </Collapsible>
                </div>
              ))}
              <br />
              <div className="space-y-10">
                <div className="flex justify-between items-center text-deep-blue border-b-2 border-white py-1">
                  <span className="font-bold text-lg uppercase">{t('price-range')}</span>
                  <button>
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                      <path fill-rule="evenodd" d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" />
                    </svg>
                  </button>
                </div>
                <div className="flex justify-center lg:justify-center">
                  <PriceRangeSlider
                    min={1}
                    max={10000}
                    onChange={value => {
                      priceRangeFromChild = value;
                    }}
                    isFilterReseted={isFilterReseted}
                  />
                </div>
                <br />
                <div className="grid justify-items-center space-y-2">
                  <Tappable
                    onClick={() => {
                      setPriceRange(priceRangeFromChild);
                      setShowCategories(!showCategories);
                    }}
                    className="py-2 px-4 bg-light-green rounded font-semibold">
                    {t('apply')}
                  </Tappable>
                  <Tappable onClick={handleResetFilter} className="py-1 px-2 bg-dark-grey text-white rounded font-semibold outline-none focus:outline-none">
                    {t('clear')}
                  </Tappable>
                </div>
              </div>
            </div>
          </CategoriesContainer>
          <div className="flex flex-col space-y-4">
            <section className="grid grid-cols-2 lg:grid-cols-3 gap-4">
              {Object.entries(displayProductsWithPagination).length === 0 ? (
                <div className="col-end-2 lg:col-end-3 bg-orange-300 w-full rounded-xl px-6 py-3 text-white">
                  <p>К сожалению данный товар не найден :( </p>
                </div>
              ) : (
                displayProductsWithPagination
              )}
            </section>
            <div className="grid justify-items-center">
              <ReactPaginate
                previousLabel={t('prev')}
                nextLabel={t('next')}
                pageCount={pageCount}
                onPageChange={changePage}
                containerClassName={'paginationBtns'}
                previousLinkClassName={'previousBtn'}
                nextLinkClassName={'nextBtn'}
                disabledClassName={'paginationDisabled'}
                activeClassName={'paginationActive'}
              />
            </div>
          </div>
        </main>
        <div className="space-y-6">
          <h2 className="font-bold text-2xl">Хиты продаж:</h2>
          <div className="grid grid-cols-2 lg:grid-cols-4 gap-4">{displayHitProducts}</div>
          <div className="grid justify-items-center">
            <ReactPaginate
              previousLabel="Пред."
              nextLabel="След."
              pageCount={hitPageCount}
              onPageChange={hitChangePage}
              containerClassName={'paginationBtns'}
              previousLinkClassName={'previousBtn'}
              nextLinkClassName={'nextBtn'}
              disabledClassName={'paginationDisabled'}
              activeClassName={'paginationActive'}
            />
          </div>
        </div>
        <div>
          <InViewPopIn>
            <NewProductsForCatalog products={products}></NewProductsForCatalog>
          </InViewPopIn>
        </div>
      </div>
    </Layout>
  );
};

export default CatalogPage;

const Collapsible = ({ title, children, subActive }) => {
  const [active, setActive] = React.useState(false);
  return (
    <div className="flex flex-col">
      <Tappable onClick={() => setActive(value => !value)} className="flex items-center justify-start w-full focus:outline-none">
        <motion.div className="mr-2" animate={{ rotate: active ? '90deg' : '0deg' }} transition={{ duration: 0.2, ease: 'easeIn' }}>
          <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={3} d="M9 5l7 7-7 7" />
          </svg>
        </motion.div>
        <span className="font-bold uppercase text-left">{title}</span>
      </Tappable>
      <AnimatePresence initial={false}>
        {active && subActive && (
          <motion.section
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: {
                opacity: 1,
                height: 'auto',
                scale: [1, 1.1, 1],
                position: 'static',
              },
              collapsed: {
                opacity: 0,
                height: 0,
              },
            }}
            transition={{ duration: 0.5, type: 'spring' }}>
            {children}
          </motion.section>
        )}
      </AnimatePresence>
    </div>
  );
};

const CategoriesContainer = styled.aside`
  display: ${props => (props.show ? 'block' : 'none')};
`;

export async function getStaticProps({ locale }) {
  try {
    const productsRes = await fetch(`http://104.248.139.194:1337/products?_locale=${locale}`);
    const hitProductsRes = await fetch(`http://104.248.139.194:1337/products?_locale=${locale}&hit_eq=true`);
    const categoriesRes = await fetch(`http://104.248.139.194:1337/categories?_locale=${locale}`);
    const products = await productsRes.json();
    const hitProducts = await hitProductsRes.json();
    const categories = await categoriesRes.json();

    const messages = {
      ...require(`../../languages/${locale}.json`),
    };

    if (!categories) {
      return {
        notFound: true,
      };
    }

    return {
      props: { products, hitProducts, categories, messages }, // will be passed to the page component as props
    };
  } catch (error) {
    console.log(error);
  }
}

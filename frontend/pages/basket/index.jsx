import React from 'react';
import Counter from '@/atoms/Counter';
import Image from 'next/image';
import Milk from '../../public/product.png';

const BasketPage = () => {
  const products = [
    { image: '', name: 'Сок Olevi абрикосовый натуральный с мякотью 2л', price: '741' },
    { image: '', name: 'Сок Olevi абрикосовый натуральный с мякотью 2л', price: '741' },
    {
      image: '',
      name: 'Сок Olevi абрикосовый натуральный с мякотью 2л, также сироп который идет как добавок к соку',
      price: '741',
    },
  ];
  return (
    <div>
      <div className="container">
        <div>
          <h1 className="font-bold text-2xl mb-6">Ваша корзина:</h1>
        </div>
      </div>
      <div className="bg-frozen-blue py-4">
        <div className="container space-y-4">
          <div className="grid grid-cols-12">
            <p className="text-xs text-dark-grey col-start-2 col-end-4">НАИМЕНОВАНИЕ ТОВАРА:</p>
            <p className="text-xs text-dark-grey col-start-8 col-end-10">КОЛИЧЕСТВО:</p>
            <p className="text-xs text-dark-grey col-start-10 col-end-12">ЦЕНА:</p>
          </div>
          {products.map(product => (
            <div className="w-full bg-white py-2 px-6 rounded-md grid grid-cols-12 items-center">
              <Image height={128} width={106.09} className="h-32 col-start-1 col-end-3 object-contain" src={Milk} alt="product" />
              <p className="col-start-3 col-end-8">{product.name}</p>
              <Counter className="col-start-8 col-end-10"></Counter>
              <span className="mb-2 col-start-10 col-end-12">
                <span className="bg-deep-blue px-4 text-2xl text-white rounded-md font-bold mr-1">{product.price}</span>
                <span className="font-semibold">тг</span>
              </span>
              <button className="col-start-12 col-end-13 focus:outline-none">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-red-800" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                </svg>
              </button>
            </div>
          ))}
          <div className="flex justify-center items-center space-x-2">
            <p className="text-xs">
              <span className="font-bold">ПРОМО-КОД</span> (ПРИ НАЛИЧИИ):{' '}
            </p>
            <input type="text" className="h-10 w-32 px-2 border border-light-grey rounded outline-none" />
          </div>
        </div>
      </div>
      <br />
      <div className="container">
        <div className="flex py-2 px-6 border border-light-grey rounded items-center">
          <div className="flex-1"></div>
          <div className="flex-1 flex items-center space-x-4">
            <p className="text-xs">
              <span className="font-bold">БОНУСЫ</span> (ПРИ НАЛИЧИИ):{' '}
            </p>
            <p className="text-xl space-x-1">
              <span className=" text-coral-red font-bold">750</span>
              <span className="text-base">тг</span>
              <span className="font-bold">БОНУСОВ</span>
            </p>
            <button className="text-xs font-bold rounded bg-light-green px-6 py-3">ПОТРАТИТЬ НА ЗАКАЗ</button>
          </div>
        </div>
        <br />
        <hr className="border-light-grey" />
        <br />
        <div className="flex">
          <div className="flex-1"></div>
          <div className="flex-1 flex space-x-10">
            <p className="font-bold text-2xl">ИТОГО СУММА К ОПЛАТЕ: </p>
            <p className="font-bold text-2xl">
              2 223 <span className="font-normal text-xl">тг</span>
            </p>
          </div>
        </div>
        <br />
        <div className="bg-frozen-blue rounded-md py-6 px-6 grid grid-cols-12 space-y-2">
          <p className="text-sm text-dark-grey col-start-2 col-end-7 row-start-1 row-end-2 self-end">ДОСТАВКА:</p>
          <p className="text-sm text-dark-grey col-start-7 col-end-9 row-start-1 row-end-2 self-end">ВРЕМЯ ДОСТАВКИ:</p>
          <p className="text-sm text-dark-grey col-start-9 col-end-12 row-start-1 row-end-2 self-end">СПОСОБЫ ОПЛАТЫ:</p>
          <select
            name="delivery-type"
            id="delivery-type"
            className="h-8  w-11/12 px-2 rounded text-sm outline-none col-start-2 col-end-7 row-start-2 row-end-3">
            <option value="del-1">ДОСТАВКА КУРЬЕРОМ</option>
            <option value="del-2">САМОВЫВОЗ</option>
          </select>
          <select
            name="delivery-time"
            id="delivery-time"
            className="h-8  w-11/12 px-2 rounded text-sm outline-none col-start-7 col-end-9 row-start-2 row-end-3">
            <option value="del-1">8:00 - 10:00</option>
            <option value="del-2">10:00 - 12:00</option>
          </select>
          <select
            name="delivery-time"
            id="delivery-time"
            className="h-8  w-12/12 px-2 rounded text-sm outline-none col-start-9 col-end-12 row-start-2 row-end-3">
            <option value="del-1">КАРТОЙ ОНЛАЙН</option>
            <option value="del-2">НАЛИЧНЫМИ</option>
          </select>
          <div className="col-start-2 col-end-7">
            <p className="text-xs">
              ВОЗМОЖНА ДОСТАВКА В ДЕНЬ ЗАКАЗА С <span className="text-salad-green">ОПЛАТОЙ ПРИ ПОЛУЧЕНИИ</span>
            </p>
            <div className="flex space-x-3">
              <p className="text-xs text-dark-grey">СУММА МИНИМАЛЬНОГО ЗАКАЗА </p>
              <span className="text-black font-bold text-xs">
                -5000<span className="text-black">ТГ</span>
              </span>
            </div>
            <div className="flex space-x-3">
              <p className="text-xs text-dark-grey">БЕСПЛАТНАЯ ДОСТАВКА ОТ СУММЫ</p>
              <span className="text-black font-bold text-xs">
                -10000<span className="text-black">ТГ</span>
              </span>
            </div>
          </div>
          <div className="col-start-9 col-end-12">
            <p className="text-xs text-dark-grey">
              ВЫ МОЖЕТЕ ОПЛАТИТЬ ЗАКАЗ БАНКОВКОЙ КАРТОЙ И НАЛИЧНЫМИ. КАССОВЫЙ ЧЕК БУДЕТ ВЫСЛАН НА ЭЛЕКТРОННУЮ ПОЧТУ, УКАЗАННУЮ В ЛИЧНОМ КАБИНЕТЕ
            </p>
          </div>
        </div>
        <br />
        <hr className="border-light-grey" />
        <br />
        <div className="flex">
          <div className="flex-1"></div>
          <div className="flex-1 flex space-x-2">
            <button className="flex items-center justify-center h-10 w-6/12 bg-light-green rounded text-xs font-bold">
              <div className="mr-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                  <path
                    fillRule="evenodd"
                    d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                    clipRule="evenodd"
                  />
                </svg>
              </div>
              ВЕРНУТЬСЯ К ПОКУПКАМ
            </button>
            <button className="flex items-center justify-center h-10 w-6/12 bg-coral-red rounded text-xs text-white font-bold">
              ОФОРМИТЬ ЗАКАЗ{' '}
              <div className="ml-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-3 w-3 text-white" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={3} d="M9 5l7 7-7 7" />
                </svg>
              </div>
            </button>
          </div>
        </div>
        <br />
      </div>
    </div>
  );
};

export default BasketPage;

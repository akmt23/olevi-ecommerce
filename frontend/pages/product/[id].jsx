import Tappable from '@/atoms/Tappable';
import ThumbnailsCarousel from '@/molecules/ThumbnailsCarousel';
import ReviewForm from '@/atoms/ReviewForm';
import React, { useState } from 'react';
import { getStrapiMedia } from 'utils/medias';
import CustomSelect from '@/atoms/CustomSelect';
import StarRatingComponent from 'react-star-rating-component';
import styled from 'styled-components';
import HitProducts from '@/organisms/HitProducts';
import Image from 'next/image';
import { useTranslations } from 'next-intl';
import BuyButtonSection from '@/organisms/BuyButtonSection';

import useDeviceDetect from '@/hooks/useDeviceDetect';
import { useRouter } from 'next/router';
const ProductPage = ({ product, hitProducts, reviews }) => {
  const { isMobile } = useDeviceDetect();
  const t = useTranslations('ProductPages');
  const router = useRouter();
  const [open, setOpen] = useState(false);
  if (!product) {
    return <h1>Нет продукта</h1>;
  }
  const { title, description, price, discount, feedbacks, images } = product || {};
  const thumbnails = images?.map(el => {
    return { url: getStrapiMedia(el?.formats?.thumbnail?.url) };
  });
  const imageSlides = images => {
    return images.map(el => (
      <div className="embla__slide bg-yellow flex justify-center items-center w-full ">
        <Image height={382} width={310} className="w-3/5 object-contain" src={getStrapiMedia(el?.url)} alt="product" />
      </div>
    ));
  };

  const locale = router.locale;
  const handleOpen = () => {
    setOpen(!open);
  };
  const discountPrice = discount ? Math.ceil(price - price * discount) : price;
  // const reviewsAvg = ratings => ratings.reduce((a, b) => a + b) / ratings.length;
  // Math.ceil(reviewsAvg(reviews.filter(review => review.rate > 0))) // This should calculate avg reviews, maybe
  return (
    <main className="container py-3">
      <div className="lg:grid lg:grid-cols-3 gap-8">
        <div className={`flex items-start ${isMobile && 'container'}`}>
          {images?.length > 0 ? (
            <ThumbnailsCarousel
              thumbnails={thumbnails}
              navButtonsEnabled
              carouselContainerClass="w-full flex flex-1"
              carouselViewPortClass="w-full flex items-center justify-center">
              {imageSlides(images)}
            </ThumbnailsCarousel>
          ) : (
            <div className="w-full border rounded-md h-full border-dark-grey flex items-center justify-center">
              <p className="text-xl uppercase font-semibold">Нет картинки</p>
            </div>
          )}
        </div>
        <div className="mt-3 lg:mt-0">
          <Tappable className="mb-4 focus:outline-none">
            <div className="uppercase text-light-grey text-xs tracking-wider flex items-center justify-center space-x-3">
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fill-rule="evenodd"
                  d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                  clip-rule="evenodd"
                />
              </svg>
              <span>{t('action-fav')}</span>
            </div>
          </Tappable>
          <h1 className="font-bold text-2xl mb-8">{title}</h1>
          <hr />
          <div className="py-8 grid gap-6 grid-cols-6 items-center">
            <span className="uppercase col-start-1 col-end-3 text-light-grey text-xs text-center">{t('price')}</span>
            <span className="col-start-3 col-end-6">
              <span className="bg-deep-blue px-4 text-2xl text-white rounded-md font-bold mr-1">{discountPrice}</span>
              <span className="font-semibold mr-2">тг</span>
              <span className=" text-lg text-light-grey line-through">{discount ? price : ''}</span>
            </span>
          </div>
          <div className="mb-4">
            <BuyButtonSection product={product}></BuyButtonSection>
          </div>
          <hr />
          <div className="py-8 grid gap-6 grid-cols-6 items-start">
            <span className="uppercase col-start-1 col-end-3 text-light-grey text-xs text-center">{t('description-title')}</span>
            <p className="col-start-3 col-end-7 text-sm tracking-wide font-semibold">{description}</p>
          </div>
        </div>
        {/* <div className="hidden lg:block bg-frozen-blue rounded-md p-6">
          <div className="mb-4">
            <span className="uppercase text-dark-grey text-sm font-semibold mb-4">{t('delivery-title')}</span>
          </div>
          <CustomSelect
            labelField="title"
            valueField="type"
            name="delivery"
            options={[
              {
                title: `${t('delivery-option-1')}`,
                type: 'courier',
              },
              {
                title: `${t('delivery-option-2')}`,
                type: 'own',
              },
            ]}
            onChange={value => console.log(value)}
          />
          <div className="mb-2"></div>
          <CustomSelect
            labelField="title"
            valueField="type"
            name="delivery"
            options={[
              {
                title: '10:00-12:00',
                type: '10:00',
              },
            ]}
            onChange={value => console.log(value)}
          />
          <div className="mb-2"></div>
          <p className="flex justify-between items-start uppercase text-sm p-2">
            <span className="w-2/3 text-dark-grey">{t('min-order')}</span>
            <span className="font-bold">- 5000тг</span>
          </p>
          <p className="flex justify-between items-start uppercase text-sm p-2">
            <span className="w-2/3 text-dark-grey">{t('free-delivery')}</span>
            <span className="font-bold">- 10000тг</span>
          </p>
          <hr className=" border-white " />
          <p className="uppercase text-sm px-2 py-6 text-dark-grey font-semibold">
            {t('same-day-delivery-p1')}
            <span className="text-salad-green">{t('same-day-delivery-p2')}</span>
          </p>
          <hr className=" border-white " />
          <div className="my-4">
            <span className="uppercase text-dark-grey text-sm font-semibold">{t('payment-method-title')}</span>
            <div className="mb-2"></div>
            <CustomSelect
              labelField="title"
              valueField="type"
              name="delivery"
              options={[
                {
                  title: `${t('card-pay')}`,
                  type: 'courier',
                },
                {
                  title: `${t('cash-pay')}`,
                  type: 'own',
                },
              ]}
              onChange={value => console.log(value)}
            />

            <p className="p-4 text-dark-grey uppercase font-semibold text-xs tracking-tight">{t('payment-method-info')}</p>
            <p className="text-center text-coral-red text-xl tracking-wide font-bold uppercase mb-4">+1% {t('bonus')}</p>
            <Tappable className="flex flex-grow justify-center items-center py-3 rounded-md bg-light-green w-full uppercase font-bold ">{t('pay')}</Tappable>
          </div>
        </div> */}
      </div>
      <section>
        <div className="flex items-center justify-between mb-4">
          <h2 className="font-bold text-2xl">
            {t('reviews-title')} <span className="text-lg font-normal ml-2">(12)</span>
          </h2>
          <Tappable onClick={handleOpen}>
            <span className="uppercase font-semibold text-xs">{t('review-action')}</span>
          </Tappable>
        </div>
        <div className={open ? 'transition-transform duration-150 ease-in-out translate-y-0' : 'transition-transform duration-150 translate-y-full hidden'}>
          <div className="flex items-end justify-between mb-4">
            <ReviewForm close={open} product={product}></ReviewForm>
          </div>
        </div>
        <div className="flex items-center space-x-4 mb-6">
          <div className="flex items-end">
            <span className=" text-dark-grey text-xs mr-2">Рейтинг:</span>
            <StarRatingComponent name="rating" editing={false} starCount={5} value={3} />
          </div>
          <div className="flex items-center space-x-2 text-sm font-semibold">
            <span className="w-8 h-8 flex items-center justify-center p-1 bg-coral-red text-white rounded-full text-lg font-bold">5</span>
            <span>/</span>
            <span>12 оценок</span>
          </div>
        </div>
        <div>
          {reviews?.length <= 0 && (
            <div className="flex justify-center">
              <Tappable onClick={handleOpen} className="w-1/2 bg-frozen-blue rounded-md p-4 text-black font-semibold flex items-center">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z"
                  />
                </svg>
                {open ? <span>Закрыть форму</span> : <span>Вы будете первым кто добавит отзыв!</span>}
              </Tappable>
            </div>
          )}
          {reviews.map((review, i) =>
            i < 3 ? (
              <div className="border border-light-grey rounded-md mb-4 lg:w-4/5 mx-auto">
                <ReviewGridContainer className="grid grid-cols-2 justify-items-center px-4 py-4 space-x-2">
                  <Image height={64} width={64} className="h-16 w-16 border rounded-full " src="/product.png" alt="avatar" />
                  <div className="flex justify-between w-full items-center text-dark-grey text-sm pr-8">
                    <span>{review.name}</span>
                    <span>{Intl.DateTimeFormat(locale, { year: 'numeric', month: 'long', day: '2-digit' }).format(review.createdAt)}</span>
                  </div>
                  <div className="col-start-2">
                    <div className="mb-4 font-semibold">
                      <p className="w-4/5">{review.text}</p>
                    </div>
                    <div className="flex items-end">
                      <StarRatingComponent name="rating" editing={false} starCount={5} value={reviews[0].rate} />
                      <span className="ml-2">Отлично</span>
                    </div>
                  </div>
                </ReviewGridContainer>
              </div>
            ) : (
              ''
            )
          )}
        </div>
        <hr className="border-light-grey my-6" />
      </section>
      <HitProducts products={hitProducts}></HitProducts>
    </main>
  );
};

const ReviewGridContainer = styled.div`
  display: grid;
  grid-template-columns: 80px 1fr;
  column-gap: 1rem;
  grid-template-rows: 80px 1fr;
  align-items: center;
`;

const ReviewContent = styled.div`
  display: grid;
  grid-template-rows: 70px 1fr;
`;

export async function getStaticPaths() {
  const products = await fetch(`http://104.248.139.194:1337/products?_locale=ru`).then(res => res.json());
  const paths = products.map(product => {
    return {
      params: {
        id: product.id.toString(),
      },
    };
  });
  console.log('paths', paths);
  return {
    paths,
    fallback: true,
  };
}

// This also gets called at build time
export async function getStaticProps({ params }) {
  // params contains the post `id`.
  // If the route is like /posts/1, then params.id is 1
  try {
    const product = await fetch(`http://104.248.139.194:1337/products/${params.id}?_locale=ru`).then(res => res.json());
    const reviews = await fetch(`http://104.248.139.194:1337/feedbacks?_where[product.id]_eq=${params.id}&_locale=ru`).then(res => res.json());
    const hitProducts = await fetch(`http://104.248.139.194:1337/products?_locale=ru&hit_eq=true`).then(res => res.json());
    // Pass post data to the page via props
    return {
      props: {
        product,
        hitProducts,
        reviews,
        messages: {
          ...require(`../../languages/ru.json`),
        },
      },
      revalidate: 1,
    };
  } catch (error) {
    return { notFound: true };
  }
}

// export async function getServerSideProps(context) {
//   const { id } = context.params;
//   const locale = context.locale;
//   const res = await fetch(`http://104.248.139.194:1337/products/${id}?_locale=${locale}`);
//   const hitProducts = await fetch(`http://104.248.139.194:1337/products?_locale=${locale}&hit_eq=true`);
//   const reviewsRes = await fetch(`http://104.248.139.194:1337/feedbacks?_where[product.id]_eq=${id}`);
//   const data = await res.json();
//   const hits = await hitProducts.json();
//   const reviews = await reviewsRes.json();

//   if (!data) {
//     return {
//       notFound: true,
//     };
//   }

//   return {
//     props: {
//       product: data,
//       hitProducts: hits,
//       reviews,
//       messages: {
//         ...require(`../../languages/${locale}.json`),
//       }, // will be passed to the page component as props
//     },
//   };
// }

export default ProductPage;

import React, { useState } from 'react';

import Link from 'next/link';

import HitProductCard from '@/atoms/ProductCards/Hit';
import { motion, AnimatePresence } from 'framer-motion';
import Tappable from '@/atoms/Tappable';
import Layout from 'components/templates/Layout';
import { useTranslations } from 'next-intl';
import ProductCard from '@/atoms/ProductCards';

const HitsPage = ({ products }) => {
  const t = useTranslations('ProductPages');
  return (
    <Layout>
      <div className="container">
        <h1 className="font-bold text-2xl my-6">{t('hit-products')}</h1>
        <main className="mb-8 grid grid-cols-4 gap-4">
          <section className="col-start-1 col-end-5 grid grid-cols-2 lg:grid-cols-4 gap-4">
            {products.map(product => {
              return <ProductCard type="hit" product={product}></ProductCard>;
            })}
          </section>
        </main>
      </div>
    </Layout>
  );
};

export default HitsPage;

export async function getServerSideProps({ locale }) {
  const productsRes = await fetch(`http://104.248.139.194:1337/products/?_locale=${locale}&hit_eq=true`);
  const products = await productsRes.json();

  return {
    props: {
      products,
      messages: {
        ...require(`../../languages/${locale}.json`),
      },
    }, // will be passed to the page component as props
  };
}

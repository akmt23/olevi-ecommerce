import React from 'react';
import Image from 'next/image';
import contacts from '../../public/contacts.png';
import { useTranslations } from 'next-intl';
import Link from 'next/link';

const News = () => {
  const menuList = [
    { name: 'O Компании', link: '/about' },
    { name: 'Вопросы и ответы', link: '/questions' },
    { name: 'Поставщикам', link: '/rules' },
    { name: 'Вакансии', link: '/vacancies' },
    { name: 'FAQ', link: '/faq' },
    { name: 'Условия возврата', link: '/conditions' },
    { name: 'Новости', link: '/news' },
    { name: 'Отзывы клиентов', link: '/questions' },
    { name: 'Программы лояльности', link: '/programs' },
  ];
  const newsLabels = [
    {
      image: contacts,
      date: '07 мая 2018',
      title: 'Готовимся к новому мероприятию!',
      description:
        'Мы помогаем своим клиентам не тратить время в очередях магазинов и пробках, не носить тяжелые покупки и освободить время на себя и семью. Покупать продукты можно из дома, на прогулке с ребенком, из офиса, в пробке и даже загородом!',
    },
    {
      image: contacts,
      date: '07 мая 2018',
      title: 'Готовимся к новому мероприятию!',
      description:
        'Мы помогаем своим клиентам не тратить время в очередях магазинов и пробках, не носить тяжелые покупки и освободить время на себя и семью. Покупать продукты можно из дома, на прогулке с ребенком, из офиса, в пробке и даже загородом!',
    },
    {
      image: contacts,
      date: '07 мая 2018',
      title: 'Готовимся к новому мероприятию!',
      description:
        'Мы помогаем своим клиентам не тратить время в очередях магазинов и пробках, не носить тяжелые покупки и освободить время на себя и семью. Покупать продукты можно из дома, на прогулке с ребенком, из офиса, в пробке и даже загородом!',
    },
  ];
  const t = useTranslations('NewsPage');

  return (
    <div className="space-y-5 py-10">
      <div className="container">
        <div className="flex lg:h-24">
          <div className="flex-1">
            <h1 className="font-bold text-2xl mb-6">{t('title')}</h1>
          </div>
          <div className="flex-1 hidden lg:block">
            <div className="grid grid-rows-3 opacity-0 md:opacity-100">
              <div className="grid grid-cols-3">
                {menuList.map(menu => (
                  <Link href={menu.link}>
                    <a className="flex items-center text-salad-green hover:text-black hover:underline cursor-pointer">{menu.name}</a>
                  </Link>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-frozen-blue py-10">
        <div className="container space-y-8">
          <div className="flex flex-col lg:flex-row bg-white">
            <div className="flex-1">
              <Image layout="responsive" src={contacts} alt="news" className="rounded-none" />
            </div>
            <div className="grid flex-1 p-5">
              <div className="space-y-3">
                <p className="uppercase text-sm text-dark-grey">07 мая 2018 / события</p>
                <p className="text-2xl">Готовимся к новому мероприятию!</p>
                <p className="text-lg">
                  Мы помогаем своим клиентам не тратить время в очередях магазинов и пробках, не носить тяжелые покупки и освободить время на себя и семью.
                  Покупать продукты можно из дома, на прогулке с ребенком, из офиса, в пробке и даже загородом!
                </p>
              </div>
              <div className="flex items-end justify-self-end uppercase text-salad-green text-xs cursor-pointer">
                <span className="mr-3">Подробнее</span>
                <div className="h-4 w-4 flex items-center justify-center bg-gray-200 rounded-full">
                  <svg xmlns="http://www.w3.org/2000/svg" class="text-salad-green" viewBox="0 0 20 20" fill="currentColor">
                    <path
                      fill-rule="evenodd"
                      d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                      clip-rule="evenodd"
                    />
                  </svg>
                </div>
              </div>
            </div>
          </div>
          <div className="grid lg:grid-cols-3 gap-4">
            {newsLabels.map(item => (
              <NewsCard image={item.image} date={item.date} title={item.title} description={item.description} />
            ))}
          </div>
        </div>
      </div>
      <div className="container">
        <div className="grid lg:grid-cols-2 gap-4 py-5">
          {newsLabels.map(item => (
            <SubNewsCard image={item.image} date={item.date} title={item.title} />
          ))}
        </div>
      </div>
      <div className="grid container">
        <div className="flex items-end justify-self-end uppercase text-salad-green text-xs cursor-pointer">
          <span className="mr-3">Архив новостей</span>
          <div className="h-4 w-4 flex items-center justify-center bg-gray-200 rounded-full">
            <svg xmlns="http://www.w3.org/2000/svg" class="text-salad-green" viewBox="0 0 20 20" fill="currentColor">
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </div>
        </div>
      </div>
    </div>
  );
};

export default News;

export function getStaticProps({ locale }) {
  return {
    props: {
      messages: {
        ...require(`../../languages/${locale}.json`),
      },
    },
  };
}

const NewsCard = ({ image, date, title, description }) => {
  return (
    <div className="flex flex-col bg-white">
      <div className="flex-1">
        <Image layout="responsive" src={image} alt="news" className="rounded-none" />
      </div>
      <div className="grid flex-1 p-5">
        <div className="space-y-3">
          <p className="uppercase text-sm text-dark-grey">{date}</p>
          <p className="text-2xl">{title}</p>
          <p className="text-lg">{description}</p>
        </div>
        <div className="flex items-end justify-self-end uppercase text-salad-green text-xs cursor-pointer">
          <span className="mr-3">Подробнее</span>
          <div className="h-4 w-4 flex items-center justify-center bg-gray-200 rounded-full">
            <svg xmlns="http://www.w3.org/2000/svg" class="text-salad-green" viewBox="0 0 20 20" fill="currentColor">
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </div>
        </div>
      </div>
    </div>
  );
};

const SubNewsCard = ({ image, date, title }) => {
  return (
    <div className="p-4 bg-frozen-blue grid grid-cols-3 gap-3">
      <div className="col-start-1 col-end-2">
        <Image layout="responsive" src={image} alt="news" className="rounded-full" />
      </div>
      <div className="grid space-y-2 col-start-2 col-end-4">
        <p className="uppercase text-sm text-dark-grey">{date}</p>
        <p className="text-3xl">{title}</p>
        <div className="flex items-end justify-self-end uppercase text-salad-green text-xs cursor-pointer">
          <span className="mr-3">Подробнее</span>
          <div className="h-4 w-4 flex items-center justify-center bg-gray-200 rounded-full">
            <svg xmlns="http://www.w3.org/2000/svg" class="text-salad-green" viewBox="0 0 20 20" fill="currentColor">
              <path
                fill-rule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clip-rule="evenodd"
              />
            </svg>
          </div>
        </div>
      </div>
    </div>
  );
};

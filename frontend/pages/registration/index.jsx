import React, { useState } from 'react';
import styled from 'styled-components';
import Input from 'react-phone-number-input/input';
import ru from 'react-phone-number-input/locale/ru';
import Image from 'next/image';

const RegistrationPage = () => {
  const [number, setNumber] = useState('');
  return (
    <div>
      <div className="container">
        <div className="flex h-20">
          <div className="flex-1">
            <h1 className="font-bold text-2xl mb-6">Регистрация: </h1>
          </div>
          <div className="flex justify-end flex-1">
            <div className="flex items-center flex-row space-x-2">
              <span className="text-sm">
                УЖЕ ЗАРЕГИСТРИРОВАНЫ? <button className="text-salad-green">ВХОД В СИСТЕМУ</button>
              </span>
              <button className="flex items-center justify-start focus:outline-none">
                <div className="mr-2" transition={{ duration: 0.2, ease: 'easeIn' }}>
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-3 w-3 rounded-lg bg-light-green" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={3} d="M9 5l7 7-7 7" />
                  </svg>
                </div>
              </button>
            </div>
          </div>
        </div>
        <RegistrationContainer>
          <div className="grid justify-items-center col-start-1 row-start-1 row-end-3 relative">
            <Image
              height={96}
              width={96}
              className="w-24 h-24 rounded-full"
              src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png"
              alt="avatar"
            />
            <input type="file" id="upload-file-button" className="hidden" />
            <UploadButton className="bg-salad-green h-8 w-8 p-1 rounded-full flex items-center justify-center" htmlFor="upload-file-button">
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M4 5a2 2 0 00-2 2v8a2 2 0 002 2h12a2 2 0 002-2V7a2 2 0 00-2-2h-1.586a1 1 0 01-.707-.293l-1.121-1.121A2 2 0 0011.172 3H8.828a2 2 0 00-1.414.586L6.293 4.707A1 1 0 015.586 5H4zm6 9a3 3 0 100-6 3 3 0 000 6z"
                  clipRule="evenodd"
                />
              </svg>
            </UploadButton>
            <p className="text-sm text-dark-grey">ЗАГРУЗИТЬ АВАТАР</p>
          </div>
          <div className="flex col-start-2 col-6 row-start-1 row-end-2">
            <h3 className="font-bold">ВАШИ ДАННЫЕ</h3>
          </div>
          <div className="col-start-2 col-end-4  row-start-2 row-end-3 space-y-2">
            <p className="text-sm text-dark-grey">ВАШИ ФИО:</p>
            <input type="text" className="h-10 w-11/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red" />
          </div>
          <div className="col-start-4 col-end-6 row-start-2 row-end-3 space-y-2">
            <p className="text-sm text-dark-grey">ТИП ПОЛЬЗОВАТЕЛЯ:</p>
            <select name="user-type" id="user-type" className="h-10  w-11/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red">
              <option value="pers-1">Частное лицо</option>
              <option value="pers-2">Юридическое лицо</option>
            </select>
          </div>
          <div className="flex col-start-2 col-6 row-start-3 row-end-4">
            <h3 className="self-center font-bold">КОНТАКТЫ</h3>
          </div>
          <div className="col-start-2 col-end-4 row-start-4 row-end-5 space-y-2">
            <p className="text-sm text-dark-grey">КОНТАКТНЫЙ НОМЕР:</p>
            <Input
              className="h-10 w-11/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red"
              placeholder="+7 123 456 7890"
              dafaultCountry="RU"
              value={number}
              onChange={setNumber}
              maxlength="15"
            />
          </div>
          <div className="col-start-4 col-end-6 row-start-4 row-end-5 space-y-2">
            <p className="text-sm text-dark-grey">E-MAIL:</p>
            <input type="email" className="h-10 w-11/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red" />
          </div>
          <div className="flex col-start-2 col-6 row-start-5 row-end-6">
            <h3 className="self-center font-bold">АДРЕС</h3>
          </div>
          <div className="col-start-2 col-end-4 row-start-6 row-end-7 space-y-2">
            <p className="text-sm text-dark-grey">ГОРОД:</p>
            <input type="text" className="h-10 w-11/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red" />
          </div>
          <div className="col-start-4 col-end-6 row-start-6 row-end-7 space-y-2">
            <p className="text-sm text-dark-grey">УЛИЦА, ДОМ:</p>
            <input type="text" className="h-10 w-11/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red" />
          </div>
          <div className="col-start-2 col-end-3 row-start-7 row-end-8 space-y-2">
            <p className="text-sm text-dark-grey">КВАРТИРА / ОФИС:</p>
            <input type="text" className="h-10 w-10/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red" />
          </div>
          <div className="col-start-3 col-end-4 row-start-7 row-end-8 space-y-2">
            <p className="text-sm text-dark-grey">ЭТАЖ:</p>
            <input type="text" className="h-10 w-10/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red" />
          </div>
          <div className="col-start-4 col-end-6 row-start-7 row-end-8 space-y-2">
            <p className="text-sm text-dark-grey">КОД НА ДВЕРИ:</p>
            <input type="text" className="h-10 w-5/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red" />
          </div>
        </RegistrationContainer>
      </div>
      <br />
      <div className="bg-frozen-blue h-40 flex items-center">
        <div className="container">
          <div className="grid grid-cols-5">
            <div className="space-y-2 col-start-2 col-end-4">
              <p className="text-sm text-dark-grey">ПРИДУМАЙТЕ ВАШ ПАРОЛЬ:</p>
              <input type="password" className="h-10 w-8/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red" />
            </div>
            <div className="space-y-2 col-start-4 col-end-6">
              <p className="text-sm text-dark-grey">ПОВТОРИТЕ ВАШ ПАРОЛЬ:</p>
              <input type="password" className="h-10 w-8/12 px-2 border border-light-grey rounded outline-none focus:border-coral-red" />
            </div>
          </div>
        </div>
      </div>
      <div className="h-40 flex items-center">
        <div className="container">
          <div className="grid grid-cols-5">
            <div className="col-start-2 col-end-4 space-x-2 flex items-center">
              <input type="checkbox" id="personal-data" value="personal-data" className="checked: bg-coral-red checked:border-transparent" />
              <label className="text-sm text-dark-grey" htmlFor="personal-data">
                Я СОГЛАСЕН НА <span className="text-coral-red">ОБРАБОТКУ ПЕРСОНАЛЬНЫХ ДАННЫХ</span>
              </label>
            </div>
            <div className="col-start-4 col-end-6">
              <button className="flex items-center justify-center h-10 w-8/12 bg-coral-red rounded text-sm text-white font-bold">
                ЗАРЕГИСТРИРОВАТЬСЯ{' '}
                <div className="ml-4">
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-3 w-3 text-white" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={3} d="M9 5l7 7-7 7" />
                  </svg>
                </div>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const UploadButton = styled.label`
  position: absolute;
  right: 75px;
  top: 65px;
`;

const RegistrationContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-gap: 10px;
`;

export default RegistrationPage;

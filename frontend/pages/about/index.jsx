import React, { useState } from 'react';
import styled from 'styled-components';
import Input from 'react-phone-number-input/input';
import Link from 'next/link';
import Banner from '../../public/banner2.png';
import Image from 'next/image';
import pic1 from '../../public/about4.png';
import pic2 from '../../public/about1.png';
import pic4 from '../../public/about3.png';
import pic3 from '../../public/about2.png';
import backPic1 from '../../public/about_comp_pic_1.png';
import backPic2 from '../../public/about_comp_pic_2.png';
import goal1 from '../../public/goal1.png';
import goal2 from '../../public/goal2.png';
import goal3 from '../../public/goal3.png';
import goal4 from '../../public/goal4.png';
import goal5 from '../../public/goal5.png';
import goal6 from '../../public/goal6.png';
import goal7 from '../../public/goal7.png';

import { useTranslations } from 'next-intl';

const About = () => {
  const [number, setNumber] = useState('');
  const menuList = [
    { name: 'O Компании', link: '/about' },
    { name: 'Вопросы и ответы', link: '/questions' },
    { name: 'Поставщикам', link: '/rules' },
    { name: 'Вакансии', link: '/vacancies' },
    { name: 'FAQ', link: '/faq' },
    { name: 'Условия возврата', link: '/conditions' },
    { name: 'Новости', link: '/news' },
    { name: 'Отзывы клиентов', link: '/questions' },
    { name: 'Программы лояльности', link: '/programs' },
  ];
  const t = useTranslations('AboutPage');

  return (
    <div>
      <div className="container">
        <div className="h-24 lg:flex lg:h-32 ">
          <div className="flex-1 pt-3">
            <h1 className="font-bold text-2xl mb-6">{t('greeting')}</h1>
          </div>
          <div className="lg:flex-1 hidden lg:block">
            <div className="grid grid-rows-3">
              <div className="grid grid-cols-3">
                {menuList.map(menu => (
                  <Link href={menu.link}>
                    <a className="flex items-center text-salad-green hover:text-black hover:underline cursor-pointer">{menu.name}</a>
                  </Link>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-center">
        <Image src={Banner} alt="Banner" />
      </div>
      <div className="container pt-8 space-y-10">
        <p className="text-lg lg:text-3xl font-semibold text-center lg:text-left">{t('title')}</p>
        <p className="text-md lg:text-xl font-semibold">{t('text-one')}</p>
        <p className="text-md lg:text-lg">{t('text-two')}</p>
        <div>
          <div className="flex flex-col lg:flex-row lg:space-y-0 py-4">
            <div className="flex-1 flex items-center">
              <Image src={pic1} alt="" className="w-full object-contain h-64" />
            </div>
            <div className="flex flex-1 pt-3 lg:pt-0">
              <p className="text-md lg:text-lg self-center">{t('text-three')}</p>
            </div>
          </div>
          <hr className="border-light-grey " />
          <div className="flex flex-col-reverse lg:flex-row lg:space-y-0 py-4">
            <div className="flex flex-1 pt-3 lg:pt-0">
              <p className="text-md lg:text-lg self-center ">{t('text-four')}</p>
            </div>
            <div className="flex-1 flex items-center">
              <Image src={pic2} alt="" className="w-full object-contain h-64" />
            </div>
          </div>
          <hr className="border-light-grey " />
          <div className="flex flex-col lg:flex-row lg:space-y-0 py-4">
            <div className="flex-1 flex items-center">
              <Image src={pic3} alt="" className="w-full object-contain h-64" />
            </div>
            <div className="flex flex-1 pt-3 lg:pt-0">
              <p className="text-md lg:text-lg self-center ">{t('text-five')}</p>
            </div>
          </div>
          <hr className="border-light-grey " />
          <div className="flex flex-col-reverse lg:flex-row lg:space-y-0 py-4">
            <div className="flex flex-1 pt-3 lg:pt-0">
              <p className="text-md lg:text-lg self-center">{t('text-six')}</p>
            </div>
            <div className="flex-1 flex items-center">
              <Image src={pic4} alt="" className="w-full object-contain h-64" />
            </div>
          </div>
          <hr className="border-light-grey" />
        </div>
        <div className="flex justify-center">
          <p className="text-xl text-deep-blue self-center text-center">{t('slogan')}</p>
        </div>
        <div></div>
      </div>
      <div>
        <BackgroundImageContainer className="text-white py-12">
          <div className="container">
            <div className="space-y-12">
              <div className="flex justify-center">
                <p className="text-md lg:text-2xl w-4/6 text-center">
                  <span className="font-bold">{t('pre-value-title')}</span>
                  {t('value-title')}
                </p>
              </div>
              <div className="flex flex-col space-y-24 lg:grid grid-cols-12">
                <div className="col-start-1 col-end-3 grid justify-items-center lg:self-end space-y-3">
                  <Image src={goal1} alt="Клиентоориентированность" className="" />
                  <p className="text-white uppercase text-xs font-bold">{t('value-one')}</p>
                </div>
                <div className="col-start-4 col-end-7 grid justify-items-center space-y-3">
                  <Image src={goal2} alt="Нацеленность на результат" className="" />
                  <p className="text-white uppercase text-xs font-bold">{t('value-two')} </p>
                </div>
                <div className="col-start-7 col-end-10 grid justify-items-center space-y-3">
                  <Image src={goal3} alt="Развитие и открытость" className="" />
                  <p className="text-white uppercase text-xs font-bold">{t('value-three')}</p>
                </div>
                <div className="col-start-10 col-end-12 grid justify-items-center space-y-3">
                  <Image src={goal4} alt="Развитие и открытость" className="" />
                  <p className="text-white uppercase text-xs font-bold">{t('value-four')}</p>
                </div>
              </div>
              <div className="flex flex-col space-y-24 lg:grid lg:grid-cols-10 pt-12">
                <div className="col-start-2 col-end-4 grid justify-items-center lg:self-end space-y-3">
                  <Image src={goal5} alt="Командная работа" className="" />
                  <p className="text-white uppercase text-xs font-bold">{t('value-five')}</p>
                </div>
                <div className="col-start-5 col-end-6 grid justify-items-center space-y-3">
                  <Image src={goal6} alt="Технологичность" className="" />
                  <p className="text-white uppercase text-xs font-bold">{t('value-six')}</p>
                </div>
                <div className="col-start-7 col-end-9 grid justify-items-center space-y-3">
                  <Image src={goal7} alt="Безопасность" className="" />
                  <p className="text-white uppercase text-xs font-bold">{t('value-seven')}</p>
                </div>
              </div>
            </div>
          </div>
        </BackgroundImageContainer>
      </div>
      <div className="container py-12 space-y-6">
        <p className="text-2xl">{t('text-seven')}</p>
        <p>{t('text-eight')}</p>
      </div>
      <SecondBackgroundImageContainer className="text-white py-12">
        <div className="container">
          <div className="space-y-2">
            <div className="flex justify-center">
              <p className="text-2xl text-center font-bold"> {t('question-title')}</p>
            </div>
            <div className="flex justify-center">
              <p className="text-base w-10/12 text-center">{t('question-text')}</p>
            </div>
            <div className="flex flex-col space-y-3 px-4 lg:px-12 py-8 border border-white rounded w-full lg:grid grid-cols-12 lg:items-end">
              <div className="col-start-1 col-end-5">
                <p className="uppercase text-sm">{t('number')}</p>
                <Input
                  className="h-10 w-full px-2 border text-black border-light-grey rounded outline-none"
                  placeholder="+7 123 456 7890"
                  dafaultCountry="RU"
                  value={number}
                  onChange={setNumber}
                  maxlength="15"
                />
              </div>
              <div className="col-start-6 col-end-10">
                <p className="uppercase text-sm">{t('name')}</p>
                <input className="h-10 w-full px-2 border text-black border-light-grey rounded outline-none" />
              </div>
              <div className="col-start-11 col-end-13 flex">
                <button className="h-10 w-full px-2 bg-salad-green rounded outline-none self-end">{t('send')}</button>
              </div>
            </div>
          </div>
        </div>
      </SecondBackgroundImageContainer>
    </div>
  );
};

const AbsoluteElementsContainer = styled.div`
  position: absolute;
  top: 40px;
  color: white;
`;

const BackgroundImageContainer = styled.div`
  background-image: url('/about_comp_pic_1.png');
  background-repeat: no-repeat;
  background-size: cover;
`;
const SecondBackgroundImageContainer = styled.div`
  background-image: url('/about_comp_pic_2.png');
  background-repeat: no-repeat;
  background-size: cover;
`;

export default About;

export function getStaticProps({ locale }) {
  return {
    props: {
      messages: {
        ...require(`../../languages/${locale}.json`),
      },
    },
  };
}

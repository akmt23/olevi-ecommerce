import React, { useEffect, useState } from 'react';
import Layout from 'components/templates/Layout';
import ProductCard from '@/atoms/ProductCards';
import { allItems } from 'utils/localStorage';
import { useTranslations } from 'next-intl';

const FavoritePageProducts = ({ products }) => {
  return products.length ? (
    <section className="col-start-1 col-end-5 grid grid-cols-2 lg:grid-cols-4 gap-4">
      {products.map(product => (
        <ProductCard product={product}></ProductCard>
      ))}
    </section>
  ) : (
    <section className="self-center">
      <h1 className="font-semibold text-xl my-6">Избранных продуктов нет</h1>
    </section>
  );
};

const FavoritePage = ({ messages }) => {
  const [products, setProducts] = useState([]);
  const t = useTranslations('Header');
  useEffect(() => {
    setProducts(allItems());
  }, []);
  return (
    <Layout>
      <div className="container">
        <h1 className="font-bold text-2xl my-6">{t('action-fav')}</h1>
        <main className="mb-8 grid grid-cols-4 gap-4">
          <FavoritePageProducts products={products} />
        </main>
      </div>
    </Layout>
  );
};

export default FavoritePage;

export function getStaticProps({ locale }) {
  const messages = {
    ...require(`../../languages/${locale}.json`),
  };
  return {
    props: { messages },
  };
}

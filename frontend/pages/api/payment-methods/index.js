const handler = async (req, res) => {
  const { publicToken, mode, invoice } = req.body;
  const response = await fetch(`https://payment.snipcart.com/api/public/custom-payment-gateway/validate?publicToken=${publicToken}`);
  if (!response.status === 401) {
    return {
      statusCode: 404,
      body: '',
    };
  }
  const checkoutUrl = process.env.HOST + '/cloudpay';
  let paymentMethodList = [
    {
      id: 'cloud-payments',
      name: 'Cloud Payments',
      checkoutUrl,
    },
  ];

  res.send(paymentMethodList);
};

export default handler;

import axios from 'axios';

const handler = async (req, res) => {
  const { paymentSessionId, state, transactionId } = req.body;
  const secretApiSnipcartKey = process.env.SNIPCART_SECRET;
  try {
    const response = await axios.post(
      'https://payment.snipcart.com/api/private/custom-payment-gateway/payment',
      {
        paymentSessionId: paymentSessionId,
        state: state,
        error: '',
        transactionId: transactionId,
        instructions: 'Your payment will appear on your statement in the coming days',
        links: { refunds: `<YOUR_REFUND_URL>?transactionId=${paymentSessionId}` },
      },
      {
        headers: {
          Authorization: `Bearer ${secretApiSnipcartKey}`,
          'Content-Type': 'application/json',
          'User-Agent': 'ANYTHING_WILL_WORK_HERE',
        },
      }
    );
    if (!response.status === 401) {
      throw new Error('Unathorized');
    }
    const body = response.data;
    res.send({ ok: true, returnUrl: body.returnUrl });
  } catch (error) {
    res.send({ ok: false, error });
  }
};

export default handler;

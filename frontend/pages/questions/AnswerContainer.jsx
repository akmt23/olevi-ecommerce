import React from 'react';
import CollapsibleAnswer from './CollabsibleAnswer';
import styled from 'styled-components';
import Image from 'next/image';
import SomePicture from '../../public/product.png';

const QuestionContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(16, 1fr);
`;

const AnswerContainer = ({ answer }) => {
  if (answer) {
    return (
      <CollapsibleAnswer title="ОТВЕТ: ">
        <QuestionContainer className="flex flex-row ml-4 mr-4 p-6">
          <Image
            layout="fixed"
            height={60}
            width={60}
            className="flex self-center col-start-1 col-end-2 max-h-12 max-w-12 h-12 w-12 rounded-full"
            src={SomePicture}
            alt="avatar"
          />
          <div className="flex self-center flex-col w-18 items-start row-start-1 row-end-3 lg:row-start-1 lg:row-end-1 col-start-4 col-end-12 lg:col-start-2 lg:col-end-4">
            <p className="text-dark-grey text-xs">{answer._name}</p>
            <p className="text-dark-grey text-xs">{answer._createdAt}</p>
          </div>
          <p className="row-start-3 row-end-5 lg:row-start-1 lg:row-end-1 col-start-1 col-end-13 lg:col-start-4 italic font-black">{answer.subanswer}</p>
        </QuestionContainer>
      </CollapsibleAnswer>
    );
  }
  return <div></div>;
};

export default AnswerContainer;

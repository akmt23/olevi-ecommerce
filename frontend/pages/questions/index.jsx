import React from 'react';
import Collapsible from '@/atoms/Collapsible';
import styled from 'styled-components';
import StarRatingComponent from 'react-star-rating-component';
import AnswerContainer from './AnswerContainer';
import Image from 'next/image';
import { useTranslations } from 'next-intl';
import Link from 'next/link';
import SomePicture from '../../public/product.png';

const QuestionsPage = () => {
  const t = useTranslations('QuestionsPage');

  const questions = [
    {
      avatar: { SomePicture },
      name: 'Амиржан Алимов',
      createdAt: '07 апреля 2021',
      question:
        'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.',
      answer: {
        _avatar: { SomePicture },
        _name: 'Olevimarket',
        _createdAt: '07 апреля 2021',
        subanswer:
          'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.',
      },
    },
    {
      avatar: { SomePicture },
      name: 'Johnny',
      createdAt: '07 апреля 2021',
      question: 'Попытался сделать заказ, ...',
    },
    {
      avatar: { SomePicture },
      name: 'Aмиржан Олимов',
      createdAt: '07 апреля 2021',
      question: 'Попытался сделать заказ, ...',
      answer: {
        _avatar: { SomePicture },
        _name: 'Olevimarket',
        _createdAt: '07 апреля 2021',
        subanswer: 'Попытался сделать заказ, ...',
      },
    },
  ];
  const menuList = [
    { name: 'O Компании', link: '/about' },
    { name: 'Вопросы и ответы', link: '/questions' },
    { name: 'Поставщикам', link: '/rules' },
    { name: 'Вакансии', link: '/vacancies' },
    { name: 'FAQ', link: '/faq' },
    { name: 'Условия возврата', link: '/conditions' },
    { name: 'Новости', link: '/news' },
    { name: 'Отзывы клиентов', link: '/questions' },
    { name: 'Программы лояльности', link: '/programs' },
  ];
  return (
    <div className="container">
      <div className="flex lg:h-32 mt-3">
        <div className="flex-1">
          <h1 className="font-bold text-2xl my-6 lg:my-0">{t('title')}</h1>
        </div>
        <div className="flex-1 hidden lg:block">
          <div className="grid grid-rows-3">
            <div className="grid grid-cols-3">
              {menuList.map(menu => (
                <Link href={menu.link}>
                  <a className="flex items-center text-salad-green hover:text-black hover:underline cursor-pointer">{menu.name}</a>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </div>
      <div className="mb-6">
        <hr className="border-light-grey " />
      </div>
      <main className="flex justify-center flex-col">
        {questions.map(question => (
          <Collapsible title="ВОПРОС:">
            <div className="flex flex-col">
              <QuestionContainer className="flex flex-row mb-6 mt-6 ml-4 mr-4">
                <Image
                  layout="fixed"
                  height={60}
                  width={60}
                  className="flex self-center col-start-1 col-end-2 max-h-12 max-w-12 h-12 w-12 rounded-full"
                  src={SomePicture}
                  alt="avatar"
                />
                <div className="flex self-center flex-col w-18 items-start row-start-1 row-end-3 lg:row-start-1 lg:row-end-1 col-start-4 col-end-12 lg:col-start-2 lg:col-end-4">
                  <StarRatingComponent name="rating" editing={false} starCount={5} value={3} />
                  <p className="text-dark-grey text-xs">{question.name}</p>
                  <p className="text-dark-grey text-xs">{question.createdAt}</p>
                </div>
                <p className="row-start-3 row-end-5 lg:row-start-1 lg:row-end-1 col-start-1 col-end-13 lg:col-start-4 italic font-black">{question.question}</p>
              </QuestionContainer>
              <AnswerContainer answer={question.answer} />
            </div>
          </Collapsible>
        ))}
      </main>
    </div>
  );
};

const QuestionContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(16, 1fr);
`;

export default QuestionsPage;

export function getStaticProps({ locale }) {
  return {
    props: {
      messages: {
        ...require(`../../languages/${locale}.json`),
      },
    },
  };
}

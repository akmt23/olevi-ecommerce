import React from 'react';
import { motion, AnimatePresence } from 'framer-motion';

const CollapsibleAnswer = ({ title, children }) => {
  const [active, setActive] = React.useState(false);
  return (
    <div className="flex flex-col mt-2 p-3 bg-frozen-blue">
      <button onClick={() => setActive(value => !value)} className="flex items-center justify-start w-full focus:outline-none">
        <motion.div className="mr-2" animate={{ rotate: '0deg' }} transition={{ duration: 0.2, ease: 'easeIn' }}>
          <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={3} d="M9 5l7 7-7 7" />
          </svg>
        </motion.div>
        <span className="font-thin uppercase text-dark-grey text-sm">{title}</span>
      </button>
      <AnimatePresence initial={false}>
        {active && (
          <section
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: {
                opacity: 1,
                height: 'auto',
                scale: [1, 1.1, 1],
                position: 'static',
              },
              collapsed: {
                opacity: 0,
                height: 0,
              },
            }}
            transition={{ duration: 0.5, type: 'spring' }}>
            {children}
          </section>
        )}
      </AnimatePresence>
    </div>
  );
};

export default CollapsibleAnswer;

import React, { useContext, useEffect } from 'react';
import { CloudPaymentContext } from 'plugins/Cloudpay/contextProvider';
import Tappable from '@/atoms/Tappable';
import { motion } from 'framer-motion';
import axios from 'axios';
import { useRouter } from 'next/router';
import { useTranslations } from 'next-intl';
import CloudpayScript from 'plugins/cloudpay';

const CheckoutPage = ({ paymentSession }) => {
  const t = useTranslations('CloudPay');
  const cloudpayWidget = useContext(CloudPaymentContext);
  const { invoice } = paymentSession;
  const { amount, currency, items } = invoice;
  const router = useRouter();

  const pay = ({ amount, items, currency, email, billingAddress, targetId }) => {
    const onSuccess = async transaction => {
      try {
        const payload = {
          paymentSessionId: paymentSession.id,
          state: 'processed',
          transactionId: transaction.invoiceId,
        };
        const paymentProcessUrl = '/api/payment-proccess';
        const res = await axios.post(paymentProcessUrl, payload);
        router.push(res.data.returnUrl);
      } catch (error) {}
    };
    const onFail = () => {
      console.log('fail');
    };

    const onComplete = () => {
      console.log('complete');
    };
    showPaymentWidget({
      widget: cloudpayWidget,
      publicId: 'pk_be6c046efe52f93d381bc48da3abf',
      description: 'Оплата товаров в olevi.com',
      amount,
      currency: 'KZT',
      email,
      accountId: 'user@example.com',
      invoiceId: targetId,
      skin: 'mini',
      onSuccess,
      onFail,
      onComplete,
    });
  };
  const showPaymentWidget = ({ widget, onSuccess, onFail, onComplete, ...paymentConfig }) => {
    if (!widget) return;
    widget.pay(
      'charge',
      {
        ...paymentConfig,
      },
      {
        onSuccess,
        onFail,
        onComplete,
      }
    );
  };
  return (
    <motion.div animate={{ y: [100, 0], opacity: [0, 1] }} className="bg-frozen-blue h-screen">
      <CloudpayScript></CloudpayScript>
      <div className="container flex justify-center p-4">
        <div className="bg-white w-full md:w-1/2 lg:w-xxl  rounded-3xl">
          <div className="w-full flex flex-col items-center justify-center text-center p-8">
            <img className="bg-frozen-blue w-24 h-24 rounded-full mb-4" src="/logo.svg" alt="" />
            <h1 className="text-xl font-bold text-gray-800  mb-1">Olevi, Inc.</h1>
            <p className="text-dark-grey text-sm font-medium  mb-3">{t('recipt')}</p>
            <p className="uppercase text-deep-blue text-3xl font-bold mb-4">
              {amount} {currency}
            </p>
            <div className="text-left text-dark-grey w-full mb-4">
              <p className="text-black font-semibold text-lg mb-2">{t('basket')}</p>
              <ul className="">
                {items?.map(({ name, quantity, amount }) => (
                  <li className="flex justify-between mb-2">
                    <p>
                      {name} <span className="text-coral-red font-semibold"> x {quantity}</span>
                    </p>
                    <p className="uppercase text-deep-blue font-semibold">
                      {amount} {currency}
                    </p>
                  </li>
                ))}
              </ul>
            </div>
            <Tappable onClick={() => pay(invoice)} className="w-full py-4 rounded-xl bg-deep-blue text-white font-semibold focus:outline-none">
              {t('pay')}
            </Tappable>
          </div>
        </div>
      </div>
    </motion.div>
  );
};

export async function getServerSideProps({ query }) {
  const { publicToken } = query;
  const paymentSession = await fetch(`https://payment.snipcart.com/api/public/custom-payment-gateway/payment-session?publicToken=${publicToken}`)
    .then(res => res.json())
    .catch(err => console.log(err));
  return { props: { paymentSession, publicToken } };
}

export default CheckoutPage;

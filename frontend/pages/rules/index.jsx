import React from 'react';

const Rules = () => {
  return (
    <div className="container space-y-5 py-6">
      <div className="flex">
        <div className="flex-1">
          <h1 className="font-bold text-2xl">Правила партнерства с Olevi</h1>
        </div>
        <div className="flex-1"></div>
      </div>
      <div className="space-y-5">
        <p className="text-xl font-normal">
          Если Вас заинтересовало сотрудничество с нашей компанией по поставкам вашей продукции или реализации продукции Olevi, предлагаем отправить нам
          коммерческое предложение и ознакомиться с условиями сотрудничества и типовыми договорами сотрудничества.
        </p>
        <p className="font-bold text-2xl">Коммерческое предложение:</p>
        <p className="text-lg font-normal">
          Для того чтобы отправить нам коммерческое предложение, вам необходимо заполнить специальную анкету (он-лайн форма на сайте olevimart.kz).
        </p>
      </div>
      <div className="space-y-5">
        <p className="font-bold text-xl">Анкета Партнера для заполнения:</p>
        <div className="space-y-3 ml-6">
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Полное наименование организации</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Адрес фактического склада/производства/основной ритейл точки</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Вид деятельности: производитель/дистрибьютор/импортер/эксклюзивный дистрибьютор</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>БИН</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Контактное лицо, контактные данные</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Веб-сайт организации</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Группа/тип товара: продовольственные/непродовольственные/собственное производство</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Категории товаров</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Наименования товарных знаков</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Ваши другие точки ритейла</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Коммерческие условия: ретро %, маркетинг %, консигнация (отсрочка платежа), реализация.</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 13l4 4L19 7" />
              </svg>
            </div>
            <p>Предложение по ассортименту (приложить файл с ассортиментной матрицей, с указанием среднего срока годности продуктов и цены).</p>
          </div>
        </div>
      </div>
      <div className="space-y-5">
        <p className="font-bold text-xl">Условия партнерства с Olevi:</p>
        <div className="space-y-3 ml-6">
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Качество товара соответствует требованиям законодательства РК и Таможенного союза на протяжении всего периода сотрудничества.</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Соотношение цены и качества товара. Цены на поставляемый товар являются конкурентоспособными и соответствуют требованиям рынка.</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>
              Товар имеет индивидуальную и транспортную упаковку, содержащую информацию о наименовании товара, штрих-коде, наименовании и адресе производителя,
              дате изготовления и сроке годности, условиях хранения и другую необходимую информацию.
            </p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>
              Бесперебойные поставки товара на наш распределительный склад, согласно утвержденному графику Типового договора Olevi (с указанием регулярности и
              сроков поставок, наличие поставок в праздничные и выходные дни).
            </p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Поставки товара в согласованных объемах, удовлетворяющих потребности Olevi на определенный период.</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Предполагаемый ежемесячный объем поставок / Ежемесячный прогноз продаж товара Olevi.</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Наличие у поставщика постоянного запаса товаров, для гарантии бесперебойных поставок. </p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Отсутствие условий приобретения линейки товаров (обязательная покупка одних товаров вместе с другими товарами).</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Отсутствие задолженности у Партнера по налогам и сборам.</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Информация о присутствии товара в др.торговых точках г. Алматы.</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Наличие у Партнера системы качества, срок действия системы качества и тип стандарта качества. Наличие сертификатов качества. </p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Согласие на проведение аудита производственных линий, цехов. складских помещений Партнера независимой компанией или представителем Olevi.</p>
          </div>
          <div className="flex space-x-5 items-center">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                  clipRule="evenodd"
                />
              </svg>
            </div>
            <p>Наличие системы электронного документооборота, в т.ч. электронный обмен счет-фактурами.</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Rules;

export function getStaticProps({ locale }) {
  return {
    props: {
      messages: { ...require(`../../languages/${locale}.json`) },
    },
  };
}

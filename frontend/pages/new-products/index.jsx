import React from 'react';
import Layout from 'components/templates/Layout';
import ProductCard from '@/atoms/ProductCards';
import { useRouter } from 'next/router';
import { useTranslations } from 'next-intl';

const NewProduct = ({ products }) => {
  const t = useTranslations('Organisms');
  const router = useRouter();
  return (
    <Layout>
      <div className="container">
        <h1 className="font-bold text-2xl my-6">{t('New-title')}</h1>
        <main className="mb-8 grid grid-cols-4 gap-4">
          <section className="col-start-1 col-end-5 grid grid-cols-2 lg:grid-cols-4 gap-4">
            {products.map(product => {
              if (router.query.id) {
                const isContainedInCategories = product?.categories?.filter(category => category.id == router.query.id).length > 0;
                if (isContainedInCategories) {
                  return (
                    <ProductCard
                      type="new"
                      id={product?.id}
                      description={product?.description}
                      title={product?.title}
                      price={product?.price}
                      category={product.category}
                      imgUrl={product?.image?.url}></ProductCard>
                  );
                }
              } else {
                return (
                  <ProductCard
                    type="new"
                    id={product.id}
                    price={product.price}
                    category={product.category}
                    title={product.title}
                    imgUrl={product?.image?.url}></ProductCard>
                );
              }
            })}
          </section>
        </main>
      </div>
    </Layout>
  );
};

export default NewProduct;

export async function getServerSideProps({ locale }) {
  const productsRes = await fetch(`http://104.248.139.194:1337/products?_locale=${locale}`);
  const products = await productsRes.json();

  return {
    props: {
      products,
      messages: {
        ...require(`../../languages/${locale}.json`),
      },
    }, // will be passed to the page component as props
  };
}

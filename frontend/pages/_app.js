import React, { useEffect } from 'react';
import App from 'next/app';
import Head from 'next/head';
import Base from '../components/Base';
import { getCategories } from '../utils/api';
import '../styles/index.css';
import { AnimatePresence } from 'framer-motion';
import Router from 'next/router';
import NProgress from 'nprogress'; //nprogress module
import 'nprogress/nprogress.css'; //styles of nprogress
import SnipcartPlugin from 'plugins/snipcart';
import { RecoilRoot, atom, selector, useRecoilState, useRecoilValue } from 'recoil';
import CloudpayScript from 'plugins/Cloudpay/scriptImporter';
import { CloudPaymentsProvider } from 'plugins/Cloudpay/contextProvider';
import { NextIntlProvider } from 'next-intl';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const MyApp = ({ Component, pageProps }) => {
  return (
    <RecoilRoot>
      <NextIntlProvider messages={pageProps.messages}>
        <Base categories={pageProps.categories}>
          <Head>
            <link rel="preload" href="/fonts/OpenSans/OpenSans-Regular.ttf" as="font" crossOrigin="" />
            <link rel="preload" href="/fonts/OpenSans/OpenSans-SemiBold.ttf" as="font" crossOrigin="" />
            <link rel="preload" href="/fonts/OpenSans/OpenSans-Light.ttf" as="font" crossOrigin="" />
            <link rel="preload" href="/fonts/OpenSans/OpenSans-Bold.ttf" as="font" crossOrigin="" />
            <link rel="preload" href="/fonts/OpenSans/OpenSans-LightItalic.ttf" as="font" crossOrigin="" />
            <link rel="preload" href="/fonts/Corinthia/Corinthia.ttf" as="font" crossOrigin="" />
            <link rel="preconnect" href="https://app.snipcart.com" />
            <link rel="preconnect" href="https://cdn.snipcart.com" />
            <link rel="stylesheet" href="https://cdn.snipcart.com/themes/v3.2.0/default/snipcart.css" media="nope!" onload="this.media='all'" />
          </Head>
          {/* Snipcart plugin for checkout flow */}
          <SnipcartPlugin></SnipcartPlugin>
          <AnimatePresence initial={false} exitBeforeEnter>
            <CloudPaymentsProvider>
              <Component {...pageProps} />
            </CloudPaymentsProvider>
          </AnimatePresence>
        </Base>
      </NextIntlProvider>
    </RecoilRoot>
  );
};

// getInitialProps disables automatic static optimization for pages that don't
// have getStaticProps. So [[...slug]] pages still get SSG.
// Hopefully we can replace this with getStaticProps once this issue is fixed:
// https://github.com/vercel/next.js/discussions/10949
MyApp.getInitialProps = async ctx => {
  // Calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(ctx);
  // Fetch global site settings from Strapi
  try {
    const categories = await getCategories();
    return { ...appProps, pageProps: { categories, path: ctx.pathname } };
  } catch (error) {
    // Pass the data to our page via props
    return { ...appProps, pageProps: { categories: [], path: ctx.pathname } };
  }
};

export default MyApp;

import React from 'react';
import Image from 'next/image';
import { getStrapiMedia } from 'utils/medias';

const RecipePage = ({ recipe }) => {
  return (
    <div className="container space-y-5 py-6 w-3/5 h-1/3">
      <div className="flex flex-col">
        <h3 className="font-bold text-2xl mb-1">{recipe.title}</h3>
        <p className="font-thin text-xs">{recipe.date}</p>
        <hr className="border border-dashed mb-4 dark-green" />
        <Image src={getStrapiMedia(recipe.media.url)} width={500} height={370} />
        <h4 className="font-semibold text-xl my-2">Ингридиенты</h4>
        <hr className="border border-dashed mb-4 dark-green" />
        {recipe.content.split('\n').map(line => {
          return (
            <div className="flex flex-row space-x-3 items-center align-baseline">
              <div>
                <svg xmlns="http://www.w3.org/2000/svg" className="h-10 w-10 text-deep-blue" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <circle cx="12" cy="12" r="4" />
                </svg>
              </div>
              <p className="font-normal text-base my-2">{line}</p>
            </div>
          );
        })}
        <hr className="border border-dashed mb-4 dark-green" />
      </div>
    </div>
  );
};

export async function getServerSideProps(context) {
  const { id } = context.params;
  const { locale } = context.locale;
  const res = await fetch(`http://104.248.139.194:1337/recipes/${id}`);
  // ?_locale=${locale}
  const data = await res.json();
  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: { recipe: data },
  };
}
export default RecipePage;

import React from 'react';
import Head from 'next/head';
import { getMainPage } from '../utils/api';
import BannerCarousel from '@/molecules/BannerCarousel';
import BulletPoints from '@/molecules/BulletPoints';
import PopularCategories from '@/organisms/PopularCategories';
import HitProducts from '@/organisms/HitProducts';
import NewProducts from '@/organisms/NewProducts';
import SpecialOffers from '@/organisms/SpecialOffers';
import DiscountedProducts from '@/organisms/DiscountedProducts';
import RecipesGrid from '@/organisms/RecipesGrid';
import { motion } from 'framer-motion';
import InViewPopIn from '@/atoms/InViewPopIn';
import Layout from 'components/templates/Layout';
import { useTranslations } from 'next-intl';

const HomePage = ({ specialOffer = [], slider = [], popularCategories = [], hitProducts = [], discountedProducts = [], newProducts = [], recipes = [] }) => {
  const fadeIn = {
    hidden: { opacity: 0 },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.2,
        delayChildren: 0.1,
      },
    },
  };

  const listItem = {
    hidden: { opacity: 0, y: 100, type: 'spring' },
    show: { opacity: 1, y: 0, type: 'spring' },
  };

  const t = useTranslations('MainPage');
  return (
    <Layout>
      <Head>
        <title>{t('title')}</title>
      </Head>
      <motion.div variants={fadeIn} initial="hidden" exit="hidden" animate="show" className="mt-4">
        <motion.div variants={listItem} className="mx-2 lg:mx-10">
          <BannerCarousel slides={slider}></BannerCarousel>
        </motion.div>
        <motion.div variants={listItem}>
          <PopularCategories categories={popularCategories}></PopularCategories>
        </motion.div>
        <HitProducts products={hitProducts}></HitProducts>
        <InViewPopIn>
          <SpecialOffers offers={specialOffer}></SpecialOffers>
        </InViewPopIn>
        <InViewPopIn>
          <DiscountedProducts products={discountedProducts}></DiscountedProducts>
        </InViewPopIn>
        <InViewPopIn>
          <section className="bg-frozen-blue py-8">
            <BulletPoints></BulletPoints>
          </section>
        </InViewPopIn>
        <InViewPopIn>
          <NewProducts products={newProducts}></NewProducts>
        </InViewPopIn>
        <div className="container mb-10">
          <hr className="border-light-grey " />
        </div>
        <InViewPopIn>
          <RecipesGrid recipes={recipes}></RecipesGrid>
        </InViewPopIn>
      </motion.div>
    </Layout>
  );
};

export async function getStaticProps(context) {
  const locale = context.locale;
  const { specialOffer, slider, popularCategories, hitProducts, discountsProducts, new_products, recipes, categories } = await getMainPage(locale);
  return {
    props: {
      specialOffer: specialOffer ?? [],
      slider: slider ?? [],
      popularCategories: popularCategories ?? [],
      hitProducts: hitProducts ?? [],
      discountedProducts: discountsProducts ?? [],
      newProducts: new_products ?? [],
      recipes: recipes ?? [],
      categories: categories ?? [],
      messages: {
        ...require(`../languages/${locale}.json`),
      },
    },
  };
}

export default HomePage;

import React, { useState } from 'react';
import DiscountProductCard from '@/atoms/ProductCards/Discounted';
import { motion, AnimatePresence } from 'framer-motion';
import Tappable from '@/atoms/Tappable';
import Layout from 'components/templates/Layout';
import { useTranslations } from 'next-intl';

const DiscountPage = ({ products }) => {
  const t = useTranslations('ProductPages');
  return (
    <Layout>
      <div className="container">
        <h1 className="font-bold text-2xl my-6">{t('discount-products')}</h1>
        <main className="mb-8 grid grid-cols-4 gap-4">
          <section className="col-start-1 col-end-5 grid grid-cols-2 lg:grid-cols-4 gap-4">
            {products.map(product => {
              return (
                <DiscountProductCard
                  ids={product.id}
                  description={product.description}
                  title={product.title}
                  price={product.price}
                  category={product.category}
                  imgUrl={product?.image?.url}
                  initialPrice={product.price}
                  discountPercent={9}></DiscountProductCard>
              );
            })}
          </section>
        </main>
      </div>
    </Layout>
  );
};

export default DiscountPage;

export async function getServerSideProps({ locale }) {
  const productsRes = await fetch(`http://104.248.139.194:1337/products?_locale=${locale}&discount_gt=0`);
  const products = await productsRes.json();

  return {
    props: {
      products,
      messages: {
        ...require(`../../languages/${locale}.json`),
      },
    },
    // will be passed to the page component as props
  };
}

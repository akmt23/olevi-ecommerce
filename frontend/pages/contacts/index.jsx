import React from 'react';
import { YMaps, Map, Placemark } from 'react-yandex-maps';
import styled from 'styled-components';
import Image from 'next/image';
import contPic from '../../public/contacts.png';
import { useTranslations } from 'next-intl';
import useDeviceDetect from '@/hooks/useDeviceDetect';

const Contacts = () => {
  const t = useTranslations('ContactPage');
  const { isMobile } = useDeviceDetect();
  return (
    <div>
      <div className="bg-frozen-blue py-10">
        <div className="container space-y-5">
          <div className="flex">
            <div className="flex-1">
              <h1 className="font-bold text-2xl">{t('contacts')}</h1>
            </div>
            <div className="flex-1"></div>
          </div>
          <div className="flex flex-col lg:grid grid-cols-2 col-gap-4 row-gap-4">
            <div className="col-start-1 col-end-2 row-start-1 row-end-2 rounded bg-white p-4 space-y-5">
              <p>{t('corporate')}</p>
              <div className="flex items-center space-x-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                </svg>
                <p className="text-xl">+7 771 761 55 39</p>
              </div>
              <p>{t('days')} 10:00 - 12:00</p>
              <div className="flex items-center space-x-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                  <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                </svg>
                <p className="text-salad-green">kenzhebekova.gulzat@bk.ru</p>
              </div>
            </div>
            <div className="col-start-2 col-end-3 row-start-1 row-end-2 rounded bg-white p-4 space-y-5">
              <p>{t('clients')}</p>
              <div className="flex items-center space-x-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                </svg>
                <p className="text-xl">+7 771 761 55 39</p>
              </div>
              <p>{t('days')} 10:00 - 12:00</p>
              <div className="flex items-center space-x-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                  <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                </svg>
                <p className="text-salad-green">kenzhebekova.gulzat@bk.ru</p>
              </div>
            </div>
            <div className="col-start-1 col-end-2 row-start-2 row-end-3  rounded bg-white p-4 space-y-5">
              <p>{t('providers')}</p>
              <div className="flex items-center space-x-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                </svg>
                <p className="text-xl">+7 771 761 55 39</p>
              </div>
              <p>{t('days')} 10:00 - 12:00</p>
              <div className="flex items-center space-x-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                  <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                </svg>
                <p className="text-salad-green">kenzhebekova.gulzat@bk.ru</p>
              </div>
            </div>
            <div className="col-start-2 col-end-3 row-start-2 row-end-3  rounded bg-white p-4 space-y-5">
              <p>{t('marketing')}</p>
              <div className="flex items-center space-x-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                </svg>
                <p className="text-xl">+7 771 761 55 39</p>
              </div>
              <p>{t('days')} 10:00 - 12:00</p>
              <div className="flex items-center space-x-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                  <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                </svg>
                <p className="text-salad-green">kenzhebekova.gulzat@bk.ru</p>
              </div>
            </div>
            <div className="col-start-1 col-end-2 row-start-3 row-end-4  rounded bg-white p-4 space-y-5">
              <p>{t('co-searchers')}</p>
              <div className="flex items-center space-x-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                </svg>
                <p className="text-xl">+7 771 761 55 39</p>
              </div>
              <p>{t('days')} 08:00 - 17:00</p>
              <div className="flex items-center space-x-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
                  <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                  <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                </svg>
                <p className="text-salad-green">kenzhebekova.gulzat@bk.ru</p>
              </div>
            </div>
          </div>
          <div className="w-full">
            <YMaps>
              <div className="w-full">
                <Map state={{ center: [43.26499405508108, 76.86137541657081], zoom: 14 }} width={isMobile ? 340 : 1210} height={300}>
                  <Placemark geometry={[43.26499405508108, 76.86137541657081]} />
                </Map>
              </div>
            </YMaps>
          </div>
        </div>
      </div>
      <div className="container py-10">
        <div className="flex">
          <div className="flex-1 space-y-4">
            <h1 className="font-bold text-2xl">{t('contacts')}</h1>
            <p className="uppercase text-sm">{t('client-name')}</p>
            <input type="text" className="h-10 w-11/12 px-2 border border-light-grey rounded outline-none" />
            <div className="flex flex-col lg:flex-row space-y-4 lg:space-y-0">
              <div className="flex-1 space-y-4">
                <p className="uppercase text-sm">{t('company-name')}</p>
                <input type="text" className="h-10 w-11/12 lg:w-10/12 px-2 border border-light-grey rounded outline-none" />
              </div>
              <div className="flex-1 space-y-4">
                <p className="uppercase text-sm">{t('email')}</p>
                <input type="text" className="h-10 w-11/12 lg:w-10/12 px-2 border border-light-grey rounded outline-none" />
              </div>
            </div>
            <p className="uppercase text-sm">{t('inquiry-text')}</p>
            <textarea
              name="description"
              id="description"
              cols="30"
              rows="10"
              className="w-11/12 h-32 px-2 py-2 border border-light-grey rounded outline-none"></textarea>
            <button className="py-2 px-8 bg-salad-green text-white rounded">{t('send')}</button>
          </div>
          <div className="flex-1 hidden lg:block">
            <Image src={contPic} alt="Задайте вопросы" className="h-full w-full"></Image>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contacts;

export function getStaticProps({ locale }) {
  return {
    props: {
      messages: {
        ...require(`../../languages/${locale}.json`),
      },
    },
  };
}

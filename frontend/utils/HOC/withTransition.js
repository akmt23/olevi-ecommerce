import { motion } from 'framer-motion';
import styled from 'styled-components';

const withTransition = OriginalComponent => {
  return () => (
    <>
      <OriginalComponent />
      <SlideInContainer
        className="slide-in"
        initial={{ scaleX: 0 }}
        animate={{ scaleX: 0 }}
        exit={{ scaleX: 1 }}
        transition={{ duration: 1, ease: 'easeInOut' }}
      />
      <SlideOutContainer
        className="slide-out"
        initial={{ scaleX: 1 }}
        animate={{ scaleX: 0 }}
        exit={{ scaleX: 0 }}
        transition={{ duration: 1, ease: 'easeInOut' }}
      />
    </>
  );
};

const SlideInContainer = styled(motion.div)`
  position: fixed;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
  background: red;
  transform-origin: left;
`;

const SlideOutContainer = styled(motion.div)`
  position: fixed;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
  background: red;
  transform-origin: right;
`;

export default withTransition;

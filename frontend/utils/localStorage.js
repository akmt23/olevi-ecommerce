export const setItem = (key, value) => localStorage.setItem(key, JSON.stringify(value));

export const clearItem = (key, cb) => localStorage.removeItem(key);

export const allItems = () => {
  const products = [];
  for (let i = 0; i < localStorage.length; i++) {
    let key = localStorage.key(i);
    if (key.includes('product')) products.push(JSON.parse(localStorage.getItem(key)));
  }
  return products;
};
export const getItem = key => localStorage.getItem(key);

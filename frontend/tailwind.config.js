const { colors } = require(`tailwindcss/defaultTheme`);

module.exports = {
  purge: ['./components/**/*.js', './pages/**/*.js', './components/**/*.jsx', './pages/**/*.jsx'],
  theme: {
    extend: {
      fontFamily: {
        display: 'Corinthia',
        serif: 'Open Sans',
      },
      colors: {
        primary: colors.indigo,
        'white-custom': '#FFFDFD',
        'frozen-blue': '#E6ECF4',
        'coral-red': '#EA6153',
        'dark-grey': '#7D7D7D',
        'light-grey': '#C7C7C7',
        'salad-green': '#73B94B',
        'deep-purple': '#d094ed',
        'deep-blue': '#0655a2',
        'light-blue': '#0093e1',
        'light-green': '#d5e8c7',
        'coral-red': '#ea6153',
        'light-purple': '#918ae2',
        'dark-green': '#2a928f',
        'deep-yellow': '#f2bd56',
      },
      container: {
        center: true,
        padding: {
          default: '1rem',
          md: '2rem',
        },
      },
      spacing: {
        sm: '8px',
        md: '16px',
        lg: '24px',
        xl: '300px',
        xxl: '500px',
      },
      minWidth: {
        0: '0',
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '75%',
        '2/6': '23%',
        full: '100%',
      },
      maxWidth: {
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '75%',
        '2/6': '23%',
      },
    },
  },
};

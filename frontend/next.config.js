module.exports = {
  i18n: {
    localeDetection: false,
    locales: ['ru', 'kk', 'en'],
    defaultLocale: 'ru',
  },
  images: {
    domains: ['104.248.139.194'],
  },
};

import Script from 'next/script';
import { EventEmitter } from 'utils/eventEmitter';

const CloudpayScript = () => {
  const initializeCloudpay = () => {
    const cloudPaymentsInstance = new cp.CloudPayments();
    EventEmitter.dispatch('onCloudPaymentsInitialized', cloudPaymentsInstance);
  };
  return (
    <>
      <Script async src="https://widget.cloudpayments.ru/bundles/cloudpayments" onLoad={initializeCloudpay} data-autopop="false"></Script>
    </>
  );
};

export default CloudpayScript;

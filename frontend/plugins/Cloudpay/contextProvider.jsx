import React from 'react';
import { EventEmitter } from 'utils/eventEmitter';
export const CloudPaymentContext = React.createContext();

export const CloudPaymentsProvider = props => {
  const [cloudPaymentInstance, setCloudPaymentInstance] = React.useState(null);
  React.useEffect(() => {
    EventEmitter.subscribe('onCloudPaymentsInitialized', instance => setCloudPaymentInstance(instance));
  }, []);

  return <CloudPaymentContext.Provider value={cloudPaymentInstance}>{props.children}</CloudPaymentContext.Provider>;
};

export default CloudPaymentsProvider;

import Script from 'next/script';

export const SnipcartSingleton = {
  snipcartInstance: null,
  getSnipcart: function () {
    return this.snipcartInstance;
  },
  setSnipcartInstance: function (instance) {
    this.snipcartInstance = instance;
  },
};

const SnipcartPlugin = () => {
  const initializeSnipCart = () => {
    Snipcart.api.session.setCurrency('kzt');
    Snipcart.api.session.setLanguage('ru');
    SnipcartSingleton.setSnipcartInstance(Snipcart);
  };
  return (
    <>
      <Script src="https://cdn.snipcart.com/themes/v3.2.0/default/snipcart.js" onLoad={initializeSnipCart} data-autopop="false" strategy="lazyOnload"></Script>
      <SnipCartCustomizations />
    </>
  );
};

const SnipCartCustomizations = () => {
  return (
    <div
      hidden
      id="snipcart"
      data-api-key="NGI3ODIxZWItY2MxMS00NmIyLTk3MTctOTg1ODU0ZDk3YTE4NjM3NjE2OTEzNjIyMTU2ODA4"
      data-config-add-product-behavior="none"></div>
  );
};

export default SnipcartPlugin;

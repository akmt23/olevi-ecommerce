import Script from 'next/script';

export const cloudpayments = {
  instance: null,
  getInstance: function () {
    return this.instance;
  },
  setInstance: function (instance) {
    this.instance = instance;
  },
};

const CloudpayScript = () => {
  const initializeCloudpay = () => {
    const cloudPaymentsInstance = new cp.CloudPayments();
    cloudpayments.setInstance(cloudPaymentsInstance);
  };
  return (
    <>
      <Script async src="https://widget.cloudpayments.ru/bundles/cloudpayments" onLoad={initializeCloudpay} data-autopop="false"></Script>
    </>
  );
};

export default CloudpayScript;

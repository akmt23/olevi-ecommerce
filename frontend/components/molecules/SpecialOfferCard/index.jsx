import React from 'react';
import styled from 'styled-components';
import { getStrapiMedia } from '../../../utils/medias';
import Link from 'next/link';
import Image from 'next/image';
const SpecialOfferCard = ({ offer }) => {
  return (
    <Link href={`/specials?id=${offer.category.id}`}>
      <div className="rounded-lg w-full bg-white mr-4 overflow-hidden cursor-pointer">
        <OfferImage height={400} width={450} className="relative w-full object-cover " src={getStrapiMedia(offer?.media[0]?.url)} alt="offer1" />
        <div className="relative h-40">
          <OfferCardContainer className="z-10">
            <SpecialOfferTitle title={offer?.title} subtitle={offer?.subTitle} subtitleColor={offer?.color}></SpecialOfferTitle>
          </OfferCardContainer>
        </div>
      </div>
    </Link>
  );
};

const OfferImage = styled(Image)`
  height: 400;
  object-fit: cover;
`;

const SpecialOfferTitle = ({ title, subtitle, subtitleColor }) => {
  return (
    <OfferTitleContainer>
      <OfferDisplayTitle className=" font-display text-black">{title}</OfferDisplayTitle>
      <OfferSansTitle className="text-4xl font-bold tracking-wider text-white px-2  rounded uppercase" color={subtitleColor}>
        {subtitle}
      </OfferSansTitle>
      <OfferSubtitle className="text-light-blue font-semibold text-2xl mt-2">от «Olevi»</OfferSubtitle>
    </OfferTitleContainer>
  );
};

const OfferCardContainer = styled.div`
  background-image: url(./offerBg.png);
  background-repeat: no-repeat;
  padding: 2rem 0;
  padding-top: 4rem;
  z-index: 1;
  position: absolute;
  bottom: 0;
`;

const OfferTitleContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-auto-flow: column;
  grid-auto-columns: 300px 100px;
  padding: 0 1.5rem;
`;

const OfferDisplayTitle = styled.p`
  line-height: 50px;
  z-index: 1;
  grid-column: 1/3;
  position: relative;
  font-size: 80px;
`;
const OfferSubtitle = styled.p`
  line-height: 50px;
  z-index: 1;
  grid-column: 1/3;
  position: relative;
`;
const OfferSansTitle = styled.p`
  grid-column: 2;
  grid-row: 2;
  background-color: ${({ color }) => color};
`;

export default SpecialOfferCard;

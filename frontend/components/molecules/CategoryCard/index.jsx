import React from 'react';
import { getStrapiMedia } from '../../../utils/medias';
import { motion } from 'framer-motion';
import Image from 'next/image';
import useDeviceDetect from '@/hooks/useDeviceDetect';
import Link from 'next/link';
const CategoryCard = ({ id, title, imgUrl }) => {
  const { isMobile } = useDeviceDetect();
  const getCategoryImage = () => {
    const imageSizes = {
      mobile: { height: 64, width: 64 },
      desktop: { height: 96, width: 96 },
    };
    const imageSizeProps = imageSizes[isMobile ? 'mobile' : 'desktop'];
    return <Image {...imageSizeProps} className="h-full object-contain" src={getStrapiMedia(imgUrl)} alt={title} />;
  };
  return (
    <Link href={`/catalog?id=${id}`}>
      <motion.div whileHover={{ scale: 0.9 }} className="relative h-full mr-4">
        <div className="grid grid-rows-3 p-2 md:p-6 place-items-center  bg-frozen-blue h-32 w-32 md:w-48 md:h-48  items-center justify-evenly  rounded-md">
          <div className=" row-start-1 row-end-3">{getCategoryImage()}</div>
          <span className=" text-center font-bold text-xs md:text-sm w-full md:w-full uppercase">{title}</span>
        </div>
      </motion.div>
    </Link>
  );
};

export default CategoryCard;

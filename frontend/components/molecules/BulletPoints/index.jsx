import React from 'react';
import Image from 'next/image';
import { useTranslations } from 'next-intl';

const BulletPoints = () => {
  const t = useTranslations('Molecules');

  return (
    <div className="container grid grid-cols-2 md:grid-cols-4 gap-4">
      <div className="bg-light-blue rounded-md p-4 px-8 text-white grid grid-rows-2 lg:grid-rows-1 lg:grid-cols-3 items-center justify-center">
        <p className="font-bold text-left text-xs lg:col-start-1 lg:col-end-3  uppercase lg:mr-4">{t('BulletPoints-point-1')}</p>
        <div className="flex items-center justify-center ">
          <Image height={64} width={64} className="h-16 object-contain " src="/bullet1.png" alt="bullet1" />
        </div>
      </div>
      <div className="bg-deep-purple rounded-md p-4 px-8 text-white grid grid-rows-2 lg:grid-rows-1 lg:grid-cols-3 items-center justify-center">
        <p className="font-bold text-left text-xs lg:col-start-1 lg:col-end-3 uppercase lg:mr-4">{t('BulletPoints-point-2')}</p>
        <div className="flex items-center justify-center ">
          <Image height={64} width={64} className="h-16 object-contain" src="/bullet2.png" alt="bullet1" />
        </div>
      </div>
      <div className="bg-dark-green rounded-md p-4 px-8 text-white grid grid-rows-2 lg:grid-rows-1 lg:grid-cols-3 items-center justify-center">
        <p className="font-bold text-left text-xs lg:col-start-1 lg:col-end-3  uppercase lg:mr-4">{t('BulletPoints-point-3')}</p>
        <div className="flex items-center justify-center ">
          <Image height={64} width={64} className="h-16 object-contain" src="/bullet3.png" alt="bullet1" />
        </div>
      </div>
      <div className="bg-deep-yellow rounded-md p-4 px-8 text-white grid grid-rows-2 lg:grid-rows-1 lg:grid-cols-3 items-center justify-center">
        <p className="font-bold text-left text-xs lg:col-start-1 lg:col-end-3  uppercase lg:mr-4">{t('BulletPoints-point-4')}</p>
        <div className="flex items-center justify-center">
          <Image height={64} width={64} className="h-16 object-contain" src="/bullet4.png" alt="bullet1" />
        </div>
      </div>
    </div>
  );
};

export default BulletPoints;

import React, { useEffect, useCallback, useState } from 'react';
import { useEmblaCarousel } from 'embla-carousel/react';
import styled from 'styled-components';
import { motion } from 'framer-motion';

const ThumbnailsCarousel = ({
  navButtonsEnabled,
  children,
  slidesToScroll,
  dragFree,
  carouselContainerClass,
  carouselViewPortClass,
  loop,
  auto,
  thumbnails,
}) => {
  const [emblaRef, embla] = useEmblaCarousel({ loop, slidesToScroll: slidesToScroll ?? 1, dragFree, containScroll: 'trimSnaps' });
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [scrollSnaps, setScrollSnaps] = useState([]);
  const [thumbViewportRef, emblaThumbs] = useEmblaCarousel({
    containScroll: 'keepSnaps',
    selectedClass: '',
    dragFree: true,
  });

  const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);
  const scrollTo = useCallback(index => embla && embla.scrollTo(index), [embla]);

  const onThumbClick = useCallback(
    index => {
      setSelectedIndex(index);
      if (!embla || !emblaThumbs) return;
      if (emblaThumbs.clickAllowed()) scrollTo(index);
    },
    [embla, emblaThumbs]
  );

  useEffect(() => {
    if (!embla) return;
    setScrollSnaps(embla.scrollSnapList());
  }, [embla, setScrollSnaps]);

  const NavButtons = () => {
    return (
      <React.Fragment>
        <LeftNavButton className="focus:outline-none border border-light-grey bg-white" onClick={scrollPrev}>
          <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 19l-7-7 7-7" />
          </svg>
        </LeftNavButton>
        <RightNavButton className="focus:outline-none border border-light-grey bg-white" onClick={scrollNext}>
          <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
          </svg>
        </RightNavButton>
      </React.Fragment>
    );
  };
  const carouselContainer = {
    hidden: {
      opacity: 0,
    },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.3,
      },
    },
  };

  return (
    <div className="flex flex-col">
      <div className="embla w-full  relative rounded mb-4">
        <div className={`relative embla__viewport h-xxl border border-light-grey  rounded ${carouselViewPortClass}`} ref={emblaRef}>
          <motion.div variants={carouselContainer} initial="hidden" animate="show" className={carouselContainerClass}>
            {children}
          </motion.div>
        </div>
        {navButtonsEnabled && NavButtons()}
      </div>
      <div className="embla embla--thumb">
        <div className="embla__viewport" ref={thumbViewportRef}>
          <ThumbContainer className="embla__container embla__container--thumb ">
            {scrollSnaps.map((_, index) => (
              <Thumb
                className="embla__slide p-4 border border-light-grey rounded-md h-20 w-20 object-contain"
                onClick={() => onThumbClick(index)}
                isSelected={index === selectedIndex}
                src={thumbnails[index]?.url}
                key={index}
              />
            ))}
          </ThumbContainer>
        </div>
      </div>
    </div>
  );
};

const ThumbContainer = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-auto-columns: 20%;
  column-gap: 1rem;
  justify-items: center;
`;

const Thumb = styled.img`
  opacity: ${({ isSelected }) => (isSelected ? '1' : '30%')};
`;

const CarouselNavButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.5rem;
  border-radius: 100%;
  color: black;
`;

const LeftNavButton = styled(CarouselNavButton)`
  position: absolute;
  top: 50%;
  left: 0;
  transform: translate(-50%, -50%);
`;
const RightNavButton = styled(CarouselNavButton)`
  position: absolute;
  top: 50%;
  right: 0;
  transform: translate(50%, -50%);
`;

export default ThumbnailsCarousel;

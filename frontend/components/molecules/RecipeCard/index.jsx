import React from 'react';
import { getStrapiMedia } from '../../../utils/medias';
import Image from 'next/image';
import Link from 'next/link';
import { useTranslations } from 'next-intl';

const RecipeCard = ({ recipe }) => {
  const t = useTranslations('Molecules');

  return (
    <div className="container bg-primary">
      <div className="relative">
        <Image height={294.41} width={384} className="w-full rounded-md object-cover max-w-lg" src={getStrapiMedia(recipe?.media?.url)} alt="recept" />
        <span className="bg-deep-blue px-4 text-white rounded uppercase absolute bottom-0 left-0 mb-4 ml-4 text-xs">{t('RecipeCard-video')}</span>
      </div>
      <div className="p-4 py-6">
        <div className="flex flex-col items-start mb-4">
          <span className="font-semibold text-lg">{recipe?.title}</span>
          <span className="text-dark-grey">{recipe.date}</span>
        </div>
        <Link href={`/recipe/${recipe.id}`}>
          <div className="flex items-center uppercase text-salad-green text-xs cursor-pointer">
            <span className="mr-3">{t('RecipeCard-action')}</span>
            <div className="h-4 w-4 flex items-center justify-center bg-gray-200 rounded-full">
              <svg xmlns="http://www.w3.org/2000/svg" className="text-salad-green" viewBox="0 0 20 20" fill="currentColor">
                <path
                  fill-rule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clip-rule="evenodd"
                />
              </svg>
            </div>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default RecipeCard;

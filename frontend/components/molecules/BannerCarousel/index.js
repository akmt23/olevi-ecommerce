import React, { useEffect, useCallback, useState } from 'react';
import { useEmblaCarousel } from 'embla-carousel/react';
import styled from 'styled-components';
import Carousel from '../../atoms/Carousel';
import { getStrapiMedia } from '../../../utils/medias';
import Image from 'next/image';
const BannerCarousel = ({ slides = [] }) => {
  return (
    <Carousel auto carouselContainerClass="h-full flex w-full" carouselViewPortClass="h-48 lg:h-xxl" navDotsEnabled>
      {slides.map(slide => (
        <div className="embla__slide w-full h-full bg-purple-300">
          <a href={slide.url}>
            <Image layout="fill" src={getStrapiMedia(slide.media?.url)} className="w-full object-cover"></Image>
          </a>
        </div>
      ))}
    </Carousel>
  );
};

export default BannerCarousel;

import React from 'react';
import Link from 'next/link';
import Tappable from '@/atoms/Tappable';
import Image from 'next/image';
import useDeviceDetect from '@/hooks/useDeviceDetect';
import MobileAppHeader from './mobile';
import Sidebar from '@/organisms/Sidebar';
import { useRecoilState } from 'recoil';
import { searchState } from 'data/atoms/searchAtom';
import { useRouter } from 'next/router';
import { useTranslations } from 'next-intl';

const AppHeader = ({ categories, messages }) => {
  const { isMobile } = useDeviceDetect();
  if (isMobile) {
    return (
      <>
        <MobileAppHeader></MobileAppHeader>
      </>
    );
  }
  return (
    <div>
      <TopHeader></TopHeader>
      <div className="mb-6"></div>
      <MainHeader categories={categories}></MainHeader>
    </div>
  );
};

export default AppHeader;

const MainHeader = ({ categories }) => {
  return (
    <div>
      <NavigationMenu></NavigationMenu>
      <div className="mb-4"></div>
      <HeaderMainSection></HeaderMainSection>
      <div className="mb-6"></div>
      {/* <CategoriesHeap categories={categories}></CategoriesHeap> */}
      <div className="mb-6"></div>
      {/* <div className="mb-4"></div>
        <BreadcrumbsSection></BreadcrumbsSection> */}
    </div>
  );
};

const BreadcrumbsSection = () => {
  return (
    <div className="bg-frozen-blue text-dark-grey uppercase text-xs py-2">
      <div className="container">
        <p className="flex space-x-2 items-center">
          <span>Olevi</span>
          <span className="text-xs">/</span>
          <span>Главная</span>
        </p>
      </div>
    </div>
  );
};

const CategoriesHeap = ({ categories }) => {
  return (
    <div className="container">
      <ul className="flex flex-wrap justify-start uppercase font-bold text-sm">
        {categories.map((el, index) => (
          <li className="mr-4" key={index}>
            <Link href={`/catalog?id=${el.id}`}>
              <a>{el.name}</a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

const HeaderMainSection = () => {
  return (
    <div className="container">
      <div className="flex items-center justify-between">
        <LogoSectionMain></LogoSectionMain>
        <SearchInput></SearchInput>
        <HeaderActionButtons></HeaderActionButtons>
      </div>
    </div>
  );
};

const SearchInput = () => {
  const [search, setSearch] = useRecoilState(searchState);
  return (
    <div className="rounded flex-grow flex items-center px-1 py-1 border-blue-600 border-2 mx-6 bg-white">
      <button
        onClick={() => setSearch(true)}
        placeholder="Найти товар в Olevi..."
        type="text"
        className="flex-grow focus:outline-none px-4 bg-transparent cursor-pointer w-full h-full text-light-grey flex justify-start">
        Начать поиск в Olevi
      </button>
      <div className="p-3 bg-salad-green rounded-md text-white">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
        </svg>
      </div>
    </div>
  );
};

const HeaderActionButtons = () => {
  const t = useTranslations('Header');
  return (
    <div className="flex items-center space-x-4">
      <Link href="/favorite">
        <Tappable className="flex flex-col items-center text-light-grey hover:text-black focus:outline-none">
          <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 mb-1" viewBox="0 0 20 20" fill="currentColor">
            <path
              fillRule="evenodd"
              d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
              clipRule="evenodd"
            />
          </svg>
          <span className="text-sm">{t('action-fav')} </span>
        </Tappable>
      </Link>
      <Tappable className="snipcart-checkout flex flex-col items-center text-light-grey hover:text-black focus:outline-none">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 mb-1" viewBox="0 0 20 20" fill="currentColor">
          <path d="M3 1a1 1 0 000 2h1.22l.305 1.222a.997.997 0 00.01.042l1.358 5.43-.893.892C3.74 11.846 4.632 14 6.414 14H15a1 1 0 000-2H6.414l1-1H14a1 1 0 00.894-.553l3-6A1 1 0 0017 3H6.28l-.31-1.243A1 1 0 005 1H3zM16 16.5a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zM6.5 18a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" />
        </svg>
        <span className="text-sm">{t('action-basket')} </span>
      </Tappable>
    </div>
  );
};

const LogoSectionMain = () => {
  return (
    <div className="flex items-center space-x-4">
      <Link href="/">
        <Image height={120} width={150} className="h-32 object-contain " src="/logo.svg" alt="logo" />
      </Link>
      <DeliveryInfo></DeliveryInfo>
    </div>
  );
};

const DeliveryInfo = () => {
  const t = useTranslations('Header');
  return (
    <div className="flex items-center">
      <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-salad-green mr-2" viewBox="0 0 20 20" fill="currentColor">
        <path fillRule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clipRule="evenodd" />
      </svg>
      <div className="uppercase text-sm">
        <span className="flex items-center text-salad-green ">
          <span className="mr-1">{t('city')}</span>
        </span>
        <span className="text-dark-grey">(доставка 5 апреля с 10:00)</span>
      </div>
    </div>
  );
};

const NavigationMenu = () => {
  const t = useTranslations('Header');
  return (
    <div className="container flex justify-end">
      <ul className="flex text-sm items-center justify-end space-x-8 lg:w-1/2">
        <li>
          <Link href="/about">
            <a>{t('about')}</a>
          </Link>
        </li>
        <li>
          <Link href="/catalog">
            <a>{t('catalog')}</a>
          </Link>
        </li>
        <li>
          <Link href="/rules">
            <a>{t('partering')}</a>
          </Link>
        </li>
        <li>
          <Link href="/contacts">
            <a>{t('contacts')}</a>
          </Link>
        </li>
      </ul>
    </div>
  );
};

const TopHeader = () => {
  return (
    <div className="flex w-full bg-frozen-blue py-2">
      <div className="container text-xs flex justify-between items-center">
        <LoginWelcomeTitle></LoginWelcomeTitle>
        <LanguageSwitch></LanguageSwitch>
        <SupportBadge></SupportBadge>
        <NotificationBell></NotificationBell>
      </div>
    </div>
  );
};
const LanguageSwitch = () => {
  const router = useRouter();
  const handleRoute = locale => {
    router.push(router.asPath, router.asPath, { locale: locale });
  };
  const isActive = locale => {
    return router.locale === locale;
  };

  return (
    <div className="flex items-center space-x-1 cursor-pointer">
      <span className={isActive('kk') ? 'text-coral-red' : 'text-light-grey'} onClick={() => handleRoute('kk')}>
        KAZ
      </span>
      <span>/</span>
      <span className={isActive('ru') ? 'text-coral-red' : 'text-light-grey'} onClick={() => handleRoute('ru')}>
        RUS
      </span>
      <span>/</span>
      <span className={isActive('en') ? 'text-coral-red' : 'text-light-grey'} onClick={() => handleRoute('en')}>
        ENG
      </span>
    </div>
  );
};

const SupportBadge = () => {
  const t = useTranslations('Header');
  return (
    <div className="flex items-center">
      <span className="mr-2">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-dark-grey" viewBox="0 0 20 20" fill="currentColor">
          <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
        </svg>
      </span>
      <p className="uppercase flex space-x-2">
        <span>{t('support-text')}</span>
        <span className="font-bold">+7 701 551 85 63</span>
        <span>({t('support-days')} 09:00 - 18:00)</span>
      </p>
    </div>
  );
};

const NotificationBell = () => {
  return (
    <div className="py-1 px-3 bg-light-grey text-white rounded-lg">
      <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
        />
      </svg>
    </div>
  );
};

const LoginWelcomeTitle = () => {
  const t = useTranslations('Header');
  return (
    <div className="snipcart-summary uppercase flex space-x-1">
      <Tappable className="snipcart-customer-signin flex text-coral-red align-center font-bold">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
          <path fillRule="evenodd" d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z" clipRule="evenodd" />
        </svg>
        {t('account')}
      </Tappable>
    </div>
  );
};

import React from 'react';
import Carousel from '@/atoms/Carousel';
import SpecialOfferCard from '@/molecules/SpecialOfferCard';
import useDeviceDetect from '@/hooks/useDeviceDetect';
import { useTranslations } from 'next-intl';

const SpecialOffers = ({ offers }) => {
  const { isMobile } = useDeviceDetect();
  const t = useTranslations('Organisms');
  const carouselConfigProps = {
    mobile: {
      carouselContainerClass: 'embla__container-catalog_items-1 gap-2',
      slidesToScroll: 1,
    },
    desktop: {
      carouselContainerClass: 'embla__container-catalog_items-3 gap-4',
      slidesToScroll: 3,
    },
  }[isMobile ? 'mobile' : 'desktop'];
  return (
    <section className="bg-frozen-blue mt-10">
      <div className="container pt-8 pb-4">
        <h2 className="font-bold text-xl lg:text-2xl mb-6">{t('SpecialOffers-title')}</h2>
        <Carousel {...carouselConfigProps} navButtonsEnabled dragFree>
          {offers.map(offer => (
            <SpecialOfferCard offer={offer}></SpecialOfferCard>
          ))}
        </Carousel>
      </div>
    </section>
  );
};

export default SpecialOffers;

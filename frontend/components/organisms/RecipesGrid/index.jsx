import React from 'react';
import RecipeCard from '@/molecules/RecipeCard';
import { useTranslations } from 'next-intl';

const RecipesGrid = ({ recipes }) => {
  const t = useTranslations('Organisms');
  return (
    <section className="container my-10">
      <div className="flex items-center justify-between mb-6">
        <h2 className="font-bold text-2xl mb-6">{t('Recipes')}</h2>
      </div>
      <div className="grid grid-cols-2 gap-4 lg:grid-cols-3 lg:gap-8">
        {recipes.map(recipe => (
          <RecipeCard recipe={recipe}></RecipeCard>
        ))}
      </div>
    </section>
  );
};

export default RecipesGrid;

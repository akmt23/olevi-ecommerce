import NewProductCard from '@/atoms/ProductCards/New';
import Carousel from '@/atoms/Carousel';
import React from 'react';
import { useTranslations } from 'next-intl';
import ProductCard from '@/atoms/ProductCards';

const NewProductsForCatalog = ({ products }) => {
  const t = useTranslations('Organisms');
  return (
    <section className="my-10">
      <h2 className="font-bold text-2xl mb-6">{t('New-title')}</h2>
      <Carousel carouselContainerClass="pl-4 embla__container-discount-grid gap-4" dragFree navButtonsEnabled>
        {products.map((el, i) => (
          <ProductCard type="new" product={el}></ProductCard>
        ))}
      </Carousel>
    </section>
  );
};

export default NewProductsForCatalog;

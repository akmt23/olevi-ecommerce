import React from 'react';
import { getStrapiMedia } from 'utils/medias';
import Carousel from '@/atoms/Carousel';
import CategoryCard from '@/molecules/CategoryCard';
import { motion } from 'framer-motion';
import useDeviceDetect from '@/hooks/useDeviceDetect';
import { useTranslations } from 'next-intl';

const PopularCategories = ({ categories }) => {
  const t = useTranslations('Organisms');
  const { isMobile } = useDeviceDetect();
  const slidesToScroll = isMobile ? 1 : 6;
  const listItem = {
    hidden: {
      opacity: 0,
      y: 100,
    },
    show: {
      opacity: 1,
      y: 0,
    },
  };
  return (
    <section className="container mt-10">
      <h2 className="font-bold text-xl md:text-2xl mb-6">{t('PopularCategory')}</h2>
      <Carousel carouselContainerClass="h-full flex" slidesToScroll={slidesToScroll} dragFree navButtonsEnabled>
        {categories.map(el => (
          <motion.div key={el.name} variants={listItem}>
            <CategoryCard title={el.name} imgUrl={getStrapiMedia(el.image?.url)} id={el.id}></CategoryCard>
          </motion.div>
        ))}
      </Carousel>
    </section>
  );
};

export default PopularCategories;

import React, { useEffect } from 'react';
import Carousel from '@/atoms/Carousel';
import HitProductCard from '@/atoms/ProductCards/Hit';
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import ProductCard from '@/atoms/ProductCards';
import { useTranslations } from 'next-intl';

const HitProductsCarousel = ({ products }) => {
  const t = useTranslations('Organisms');
  const controls = useAnimation();
  const { ref, inView } = useInView();

  useEffect(() => {
    if (inView) {
      controls.start('show');
    }
  }, [controls, inView]);

  const listItem = {
    hidden: {
      opacity: 0,
      y: 100,
    },
    show: {
      opacity: 1,
      y: 0,
    },
  };
  return (
    <section className="container mt-10">
      <h2 className="font-bold text-2xl mb-6">{t('Hits-title')}</h2>
      <Carousel carouselContainerClass="h-full flex" slidesToScroll={4} dragFree navButtonsEnabled>
        {products.map((product, i) => (
          <motion.div variants={listItem}>
            <ProductCard type="hit" product={product}></ProductCard>
          </motion.div>
        ))}
      </Carousel>
    </section>
  );
};

export default HitProductsCarousel;

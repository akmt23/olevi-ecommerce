import React from 'react';
import { AnimatePresence, motion } from 'framer-motion';
import Tappable from '@/atoms/Tappable';
import Link from 'next/link';
import { useTranslations } from 'next-intl';
import { useRecoilState } from 'recoil';
import { searchState } from 'data/atoms/searchAtom';

const Sidebar = ({ visible = false, onChange = () => {} }) => {
  const t = useTranslations('Organisms');
  const onCloseClick = () => onChange(visible => !visible);
  const [search, setSearch] = useRecoilState(searchState);

  return (
    <AnimatePresence>
      {visible && (
        <>
          <motion.div
            initial={{ x: '100%' }}
            animate={{
              x: 0,
            }}
            exit={{
              x: '100%',
            }}
            transition={{ type: 'spring', bounce: 0, duration: 0.4 }}
            className="fixed bg-frozen-blue text-black shadow-lg top-0 right-0 w-full max-w-sm h-screen p-5 z-10">
            <SidebarActions onCloseClick={onCloseClick} onFavoritesClick={onCloseClick} />
            <section className="mb-8 snipcart-summary">
              <Tappable className="snipcart-customer-signin w-full flex items-center justify-center my-3">
                <div className="w-full bg-salad-green text-white font-semibold flex items-center justify-center p-2 rounded-md">
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                    <path fillRule="evenodd" d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z" clipRule="evenodd" />
                  </svg>
                  <span className="text-sm">{t('Sidebar-account')}</span>
                </div>
              </Tappable>
              <SearchInput
                onClick={() => {
                  onCloseClick();
                  setSearch(true);
                }}
              />
            </section>
            <NavigationMenu />
          </motion.div>
          <motion.div
            initial={{ opacity: 0 }}
            animate={{
              opacity: 1,
            }}
            exit={{
              opacity: 0,
            }}
            transition={{ type: 'spring', bounce: 0, duration: 0.2 }}
            onClick={onCloseClick}
            className="bg-transparent px-5 fixed h-full w-full flex items-center justify-center top-0 left-0"
          />
        </>
      )}
    </AnimatePresence>
  );
};

const SidebarActions = ({ onCloseClick, onFavoritesClick }) => {
  return (
    <div className="w-full flex justify-between items-center">
      <button onClick={onFavoritesClick} className="text-deep-blue h-8 w-8 flex flex-col items-center justify-end">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mb-1" viewBox="0 0 20 20" fill="currentColor">
          <path
            fill-rule="evenodd"
            d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
            clip-rule="evenodd"
          />
        </svg>
      </button>
      <button onClick={onCloseClick} className=" text-black h-8 w-8  flex items-center justify-start">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
          <path
            fillRule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      </button>
    </div>
  );
};

const SearchInput = ({ onClick }) => {
  return (
    <button onClick={onClick} className="flex-grow w-full flex items-center px-4 py-2 rounded-md  bg-white">
      <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
        <path
          fillRule="evenodd"
          d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
          clipRule="evenodd"
        />
      </svg>
      <input placeholder="Начать поиск в Olevi" type="text" disabled className="flex-grow focus:outline-none ml-2 bg-transparent placeholder-deep-blue" />
    </button>
  );
};

const NavigationMenu = () => {
  const t = useTranslations('Organisms');
  return (
    <section>
      <nav>
        <ul className="space-y-6 text-sm text-deep-blue">
          <li>
            <Link href="/about">
              <div className="flex items-center w-full font-semibold space-x-2 ">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                <span>{t('Sidebar-about')}</span>
              </div>
            </Link>
          </li>
          <li>
            <Link href="/catalog">
              <div className="flex items-center w-full font-semibold space-x-2 ">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M16 11V7a4 4 0 00-8 0v4M5 9h14l1 12H4L5 9z" />
                </svg>
                <span>{t('Sidebar-catalog')}</span>
              </div>
            </Link>
          </li>
          <li>
            <Link href="/delivery">
              <div className="flex items-center w-full font-semibold space-x-2 ">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path d="M9 17a2 2 0 11-4 0 2 2 0 014 0zM19 17a2 2 0 11-4 0 2 2 0 014 0z" />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="M13 16V6a1 1 0 00-1-1H4a1 1 0 00-1 1v10a1 1 0 001 1h1m8-1a1 1 0 01-1 1H9m4-1V8a1 1 0 011-1h2.586a1 1 0 01.707.293l3.414 3.414a1 1 0 01.293.707V16a1 1 0 01-1 1h-1m-6-1a1 1 0 001 1h1M5 17a2 2 0 104 0m-4 0a2 2 0 114 0m6 0a2 2 0 104 0m-4 0a2 2 0 114 0"
                  />
                </svg>
                <span>{t('Sidebar-delivery')}</span>
              </div>
            </Link>
          </li>
          <li>
            <Link href="/rules">
              <div className="flex items-center w-full font-semibold space-x-2 ">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M10 21h7a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v11m0 5l4.879-4.879m0 0a3 3 0 104.243-4.242 3 3 0 00-4.243 4.242z"
                  />
                </svg>
                <span>{t('Sidebar-rules')}</span>
              </div>
            </Link>
          </li>
          <li>
            <Link href="/contacts">
              <div className="flex items-center w-full font-semibold space-x-2 ">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z"
                  />
                </svg>
                <span>{t('Sidebar-contacts')}</span>
              </div>
            </Link>
          </li>
        </ul>
      </nav>
    </section>
  );
};

export default Sidebar;

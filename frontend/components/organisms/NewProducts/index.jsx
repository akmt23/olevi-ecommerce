import NewProductCard from '@/atoms/ProductCards/New';
import Carousel from '@/atoms/Carousel';
import React from 'react';
import ActionLink from '@/atoms/ActionLink';
import useDeviceDetect from '@/hooks/useDeviceDetect';
import ProductCard from '@/atoms/ProductCards';
import { useTranslations } from 'next-intl';

const NewProducts = ({ products }) => {
  const { isMobile } = useDeviceDetect();
  const t = useTranslations('Organisms');

  const carouselConfigProps = {
    desktop: {
      carouselContainerClass: 'pl-4 embla__container-catalog_items-4 gap-4',
      slidesToScroll: 4,
    },
    mobile: {
      carouselContainerClass: 'pl-4 embla__container-catalog_items-2 gap-2',
      slidesToScroll: 2,
    },
  }[isMobile ? 'mobile' : 'desktop'];
  return (
    <section className="container my-10">
      <div className="flex items-center justify-between">
        <h2 className="font-bold text-2xl mb-6">{t('New-title')}</h2>
        <ActionLink href="/new-products" title={t('New-link-title')}></ActionLink>
      </div>
      <Carousel {...carouselConfigProps} dragFree navButtonsEnabled>
        {products.map((el, i) => (
          <ProductCard type="new" product={el}></ProductCard>
        ))}
      </Carousel>
    </section>
  );
};

export default NewProducts;

import React from 'react';
import Carousel from '@/atoms/Carousel';
import DiscountProductCard from '@/atoms/ProductCards/Discounted';
import Link from 'next/link';
import ActionLink from '@/atoms/ActionLink';
import ProductCard from '@/atoms/ProductCards';
import useDeviceDetect from '@/hooks/useDeviceDetect';
import { useTranslations } from 'next-intl';

const DiscountedProducts = ({ products }) => {
  const t = useTranslations('Organisms');

  const { isMobile } = useDeviceDetect();
  const carouselConfigProps = {
    desktop: {
      carouselContainerClass: 'pl-4 embla__container-catalog_items-4 gap-4',
      slidesToScroll: 4,
    },
    mobile: {
      carouselContainerClass: 'pl-4 embla__container-catalog_items-2 gap-2',
      slidesToScroll: 2,
    },
  }[isMobile ? 'mobile' : 'desktop'];
  return (
    <section className="container my-10">
      <div className="flex items-center justify-between mb-6">
        <Link href="/discount">
          <h2 className="cursor-pointer font-bold text-xl lg:text-2xl  text-coral-red">{t('Discounted-title')}</h2>
        </Link>
        <ActionLink href="/discount" title={t('Discounted-link-title')}></ActionLink>
      </div>
      <Carousel {...carouselConfigProps} dragFree navButtonsEnabled>
        {products.map((el, i) => (
          <ProductCard type="discount" product={el}></ProductCard>
        ))}
      </Carousel>
    </section>
  );
};

export default DiscountedProducts;

import React, { useEffect } from 'react';
import HitProductCard from '@/atoms/ProductCards/Hit';
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import { useTranslations } from 'next-intl';

const HitProductsForCatalog = ({ products }) => {
  const t = useTranslations('Organisms');
  const controls = useAnimation();
  const { ref, inView } = useInView();

  useEffect(() => {
    if (inView) {
      controls.start('show');
    }
  }, [controls, inView]);

  const container = {
    hidden: {
      opacity: 0,
    },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.3,
      },
    },
  };

  const listItem = {
    hidden: {
      opacity: 0,
      y: 100,
    },
    show: {
      opacity: 1,
      y: 0,
    },
  };
  return (
    <div>
      <h2 className="font-bold text-2xl mb-6">{t('Hits-title')}</h2>
      <motion.div ref={ref} variants={container} initial="hidden" animate={controls} className="grid grid-cols-4 gap-4">
        {products.map((el, i) => (
          <motion.div variants={listItem}>
            <HitProductCard title={el.title} price={el.price} category={'Молочная продукция'} imgUrl={el.image?.url}></HitProductCard>
          </motion.div>
        ))}
      </motion.div>
    </div>
  );
};

export default HitProductsForCatalog;

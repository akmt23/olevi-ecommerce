import React, { useState } from 'react';
import Counter from '@/atoms/Counter';
import Tappable from '@/atoms/Tappable';
import { motion } from 'framer-motion';
import useDeviceDetect from '@/hooks/useDeviceDetect';
import { getStrapiMedia } from 'utils/medias';
import { useTranslations } from 'next-intl';

const BuyButtonSection = ({ type = 'base', product = {} }) => {
  const t = useTranslations('Organisms');
  const { isMobile } = useDeviceDetect();
  const [itemsCount, setItemsCount] = useState(1);
  const [isVisible, setIsVisible] = useState(false);
  const variants = {
    open: { opacity: 1, scale: [0.5, 1.2, 1] },
    closed: { opacity: 0, scale: 0 },
  };
  const BADGE_CLOSE_DELAY = 1000;

  const onCartButtonClick = async () => {
    startAddedToCartBadgeAnimation();
  };

  const startAddedToCartBadgeAnimation = () => {
    setIsVisible(true);
    return setTimeout(() => {
      setIsVisible(false);
    }, BADGE_CLOSE_DELAY);
  };

  const CartIcon = () => {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 " viewBox="0 0 20 20" fill="currentColor">
        <path
          fillRule="evenodd"
          d="M10 2a4 4 0 00-4 4v1H5a1 1 0 00-.994.89l-1 9A1 1 0 004 18h12a1 1 0 00.994-1.11l-1-9A1 1 0 0015 7h-1V6a4 4 0 00-4-4zm2 5V6a2 2 0 10-4 0v1h4zm-6 3a1 1 0 112 0 1 1 0 01-2 0zm7-1a1 1 0 100 2 1 1 0 000-2z"
          clipRule="evenodd"
        />
      </svg>
    );
  };

  if (type === 'mini')
    return (
      <div className="relative">
        <Tappable
          data-item-id={product.id}
          data-item-price={product.price}
          data-item-url={`products/${product.id}`}
          data-item-description={product.description}
          data-item-image={getStrapiMedia(product?.image?.imgUrl)}
          data-item-name={product.title}
          data-item-quantity={itemsCount}
          onClick={onCartButtonClick}
          className="snipcart-add-item flex justify-center items-center py-2 rounded-md bg-light-green  w-full uppercase font-bold text-xs focus:outline-none text-black">
          <CartIcon></CartIcon>
        </Tappable>
        <motion.div
          animate={isVisible ? 'open' : 'closed'}
          variants={variants}
          transition={{ type: 'spring', duration: 0.5 }}
          className="bg-salad-green w-full h-full absolute top-0 left-0 rounded-md flex items-center justify-center cursor-pointer">
          <span className="text-white font-semibold text-xs lg:text-base uppercase">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
              <path
                fillRule="evenodd"
                d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </span>
        </motion.div>
      </div>
    );

  return (
    <div className="w-full relative grid grid-rows-2 lg:grid-rows-1 lg:grid-cols-6 gap-1">
      <div className="lg:col-start-1 lg:col-end-3">
        <Counter initial={itemsCount} onChange={value => setItemsCount(value)}></Counter>
      </div>
      <Tappable
        data-item-id={product?.id}
        data-item-price={product?.price}
        data-item-url={`/product/${product?.id}`}
        data-item-description={product?.description}
        data-item-image={getStrapiMedia(product?.image?.imgUrl)}
        data-item-name={product?.title}
        data-item-quantity={itemsCount}
        onClick={onCartButtonClick}
        className="snipcart-add-item lg:col-start-3 lg:col-end-7  flex flex-grow justify-center items-center py-2 rounded-md bg-light-green w-full uppercase font-bold text-xs focus:outline-none">
        <CartIcon></CartIcon>
        <span className="ml-1">{t('BuyButton-basket')}</span>
      </Tappable>
      <motion.div
        animate={isVisible ? 'open' : 'closed'}
        variants={variants}
        transition={{ type: 'spring', duration: 0.5 }}
        className="bg-salad-green w-full h-full absolute bot-0 left-0 rounded-md flex items-center justify-center cursor-pointer">
        <span className="text-white font-semibold text-xs lg:text-base uppercase">
          {t('BuyButton-added')}
          {itemsCount}!
        </span>
      </motion.div>
    </div>
  );
};

export default BuyButtonSection;

import React, { useEffect } from 'react';
import HitProductCard from '@/atoms/ProductCards/Hit';
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import { useRouter } from 'next/router';
import Link from 'next/link';
import ActionLink from '@/atoms/ActionLink';
import Carousel from '@/atoms/Carousel';
import useDeviceDetect from '@/hooks/useDeviceDetect';
import ProductCard from '@/atoms/ProductCards';
import { useTranslations } from 'next-intl';

const HitProducts = ({ products }) => {
  const t = useTranslations('Organisms');
  const controls = useAnimation();
  const { ref, inView } = useInView();
  const router = useRouter();
  const { isMobile } = useDeviceDetect();

  useEffect(() => {
    if (inView) {
      controls.start('show');
    }
  }, [controls, inView]);

  const listItem = {
    hidden: {
      opacity: 0,
      y: 100,
    },
    show: {
      opacity: 1,
      y: 0,
    },
  };

  const MobileProducts = () => (
    <Carousel carouselContainerClass="pl-4 embla__container-catalog_items-2 gap-2" dragFree navButtonsEnabled>
      {products.map((el, i) => (
        <motion.div key={i} variants={listItem}>
          <ProductCard type="hit" product={el}></ProductCard>
        </motion.div>
      ))}
    </Carousel>
  );

  const DesktopProducts = () => (
    <div className="grid grid-cols-2 lg:grid-cols-4 row-gap-4 gap-2 lg:gap-4 justify-items-stretch ">
      {products.map((el, i) => (
        <motion.div key={i} variants={listItem}>
          <ProductCard type="hit" product={el}></ProductCard>
        </motion.div>
      ))}
    </div>
  );

  const Products = () => (isMobile ? <MobileProducts /> : <DesktopProducts />);

  return (
    <section className="container mt-10">
      <div className="flex items-center justify-between mb-6">
        <Link className="flex items-center" href="/hits">
          <h2 className="cursor-pointer font-bold text-xl lg:text-2xl ">{t('Hits-title')}</h2>
        </Link>
        <ActionLink href="/hits" title={t('Hits-link-title')}></ActionLink>
      </div>
      <Products></Products>
    </section>
  );
};

export default HitProducts;

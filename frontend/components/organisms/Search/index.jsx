import React, { useEffect } from 'react';
import { InstantSearch, SearchBox, Hits, Highlight, connectSearchBox, connectHits } from 'react-instantsearch-dom';
import { instantMeiliSearch } from '@meilisearch/instant-meilisearch';
import ProductCard from '@/atoms/ProductCards';
import { motion, AnimatePresence } from 'framer-motion';
import styled from 'styled-components';
import Link from 'next/link';
import { useRecoilState } from 'recoil';
import { searchState } from 'data/atoms/searchAtom';
import { useRouter } from 'next/router';
import BuyButtonSection from '../BuyButtonSection';
const searchClient = instantMeiliSearch('http://104.248.139.194:7700', '', {
  primaryKey: 'id',
  paginationTotalHits: 6,
});

const SearchInput = ({ currentRefinement, isSearchStalled, refine }) => {
  return (
    <form noValidate action="" role="search">
      <div className="flex-grow flex items-center px-4 py-2 rounded-md  bg-white">
        <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 text-deep-blue" viewBox="0 0 20 20" fill="currentColor">
          <path
            fillRule="evenodd"
            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
            clipRule="evenodd"
          />
        </svg>
        <input
          autoFocus
          type="search"
          value={currentRefinement}
          onChange={event => refine(event.currentTarget.value)}
          placeholder="Найти товар в Olevi..."
          className="flex-grow focus:outline-none ml-2 bg-transparent placeholder-deep-blue"
        />
      </div>
    </form>
  );
};

const InstantSearchInput = connectSearchBox(SearchInput);

const CustomHits = ({ hits }) => {
  const [search, setSearch] = useRecoilState(searchState);
  const router = useRouter();
  const getCategory = hit => {
    return hit?.categories?.[0]?.name;
  };

  return (
    <div className="bg-white p-2 rounded-md space-y-2">
      <SearchContainer className="flex flex-col space-y-4 max overflow-auto">
        {hits.length === 0 && <div>Ничего не нашли(</div>}
        {hits.map(product => (
          <li className="p-4 rounded-md  bg-gray-200 hover:bg-deep-blue hover:text-white ease-in-out transition-all duration-150 cursor-pointer grid grid-cols-6">
            <Link href={`product/${product.id}`}>
              <div className="flex items-center col-start-1 col-end-6">
                <p className="font-semibold truncate text-xs lg:text-base">{product?.title}</p>
              </div>
            </Link>
            <BuyButtonSection type="mini" product={product}></BuyButtonSection>
          </li>
        ))}
      </SearchContainer>
    </div>
  );
};
const InstantCustomHits = connectHits(CustomHits);

const SearchContainer = styled.ol`
  max-height: 800px;
  min-height: 200px;
  transition: height 0.2s ease-out;
`;

const Search = ({ visible = false, onChange = () => {} }) => {
  const onCloseClick = () => onChange(visible => !visible);

  return (
    <Modal modal={visible} onCloseClick={onCloseClick}>
      <div className="py-6">
        <InstantSearch indexName="product" searchClient={searchClient}>
          <InstantSearchInput />
          <hr className="text-light-grey mx-2 my-4" />
          <InstantCustomHits />
        </InstantSearch>
      </div>
    </Modal>
  );
};

function Modal({ modal, onCloseClick, children }) {
  useEffect(() => {
    const close = e => {
      if (e.keyCode === 27) {
        onCloseClick(false);
      }
    };
    window.addEventListener('keydown', close);
    return () => window.removeEventListener('keydown', close);
  }, [onCloseClick]);
  return (
    <AnimatePresence>
      {modal && (
        <div className="px-5 fixed h-full w-full flex items-center justify-center top-0 left-0">
          <motion.div
            initial={{ scale: 0, opacity: 0 }}
            animate={{
              scale: 1,
              opacity: 1,
            }}
            exit={{
              scale: 0.2,
              opacity: 0,
            }}
            transition={{ type: 'spring', duration: 0.5 }}
            className="absolute z-10 px-4 bg-white  w-11/12 container rounded-md text-black ">
            <button onClick={onCloseClick} className="absolute top-0 right-0 m-4">
              <div className="border border-light-grey text-light-grey font-semibold text-xs uppercase p-1 rounded-md">Esc</div>
            </button>
            {children}
          </motion.div>
          <motion.div
            initial={{ opacity: 0 }}
            animate={{
              opacity: 1,
            }}
            exit={{
              opacity: 0,
            }}
            transition={{ type: 'spring', bounce: 0, duration: 0.2 }}
            onClick={onCloseClick}
            className=" bg-gray-700  bg-opacity-25 px-5 fixed h-full w-full flex items-center justify-center top-0 left-0"
          />
        </div>
      )}
    </AnimatePresence>
  );
}
function Hit(props) {
  return <Highlight attribute="title" hit={props.hit} />;
}
export default Search;

import React from 'react';
import { motion } from 'framer-motion';
import { setItem, getItem, clearItem } from 'utils/localStorage';

const HeartButton = ({ product }) => {
  const handleFavorite = () => {
    if (getItem(`product${product.id}`)) {
      clearItem(`product${product.id}`);
    } else {
      setItem(`product${product.id}`, product);
    }
  };
  return (
    <motion.button whileHover={{ scale: 1.4 }} whileTap={{ scale: 0.8 }} onClick={handleFavorite} className={`text-light-grey focus:outline-none`}>
      <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" viewBox="0 0 20 20" fill="currentColor">
        <path
          fill-rule="evenodd"
          d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
          clip-rule="evenodd"
        />
      </svg>
    </motion.button>
  );
};
export default HeartButton;

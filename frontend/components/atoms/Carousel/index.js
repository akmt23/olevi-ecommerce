import React, { useEffect, useCallback, useState } from 'react';
import { useEmblaCarousel } from 'embla-carousel/react';
import styled from 'styled-components';
import { useRecursiveTimeout } from './useRecursiveTimeout';
import Tappable from '../Tappable';
import { motion } from 'framer-motion';

const AUTOPLAY_INTERVAL = 4000;

const Carousel = ({ navDotsEnabled, navButtonsEnabled, children, slidesToScroll, dragFree, carouselContainerClass, carouselViewPortClass, loop, auto }) => {
  const [emblaRef, embla] = useEmblaCarousel({ loop, slidesToScroll: slidesToScroll ?? 1, dragFree, containScroll: 'trimSnaps' });
  const [prevBtnEnabled, setPrevBtnEnabled] = useState(false);
  const [nextBtnEnabled, setNextBtnEnabled] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [scrollSnaps, setScrollSnaps] = useState([]);

  const autoplay = useCallback(() => {
    if (!auto) return;
    if (!embla) return;
    if (embla.canScrollNext()) {
      embla.scrollNext();
    } else {
      embla.scrollTo(0);
    }
  }, [embla]);

  const { play, stop } = useRecursiveTimeout(autoplay, AUTOPLAY_INTERVAL);

  const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
  const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);
  const scrollTo = useCallback(index => embla && embla.scrollTo(index), [embla]);

  const onSelect = useCallback(() => {
    if (!embla) return;
    setSelectedIndex(embla.selectedScrollSnap());
    setPrevBtnEnabled(embla.canScrollPrev());
    setNextBtnEnabled(embla.canScrollNext());
    stop();
  }, [embla, setSelectedIndex]);

  useEffect(() => {
    if (!embla) return;
    onSelect();
    setScrollSnaps(embla.scrollSnapList());

    embla.on('select', onSelect);
    embla.on('pointerDown', stop);
  }, [embla, onSelect, stop, setScrollSnaps]);

  useEffect(() => {
    play();
  }, [play]);

  const NavButtons = () => {
    return (
      <React.Fragment>
        <LeftNavButton className="focus:outline-none border border-light-grey bg-white" onClick={scrollPrev}>
          <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 md:h-6 md:w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 19l-7-7 7-7" />
          </svg>
        </LeftNavButton>
        <RightNavButton className="focus:outline-none border border-light-grey bg-white" onClick={scrollNext}>
          <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 md:h-6 md:w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
          </svg>
        </RightNavButton>
      </React.Fragment>
    );
  };

  const carouselContainer = {
    hidden: {
      opacity: 0,
    },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.3,
      },
    },
  };

  return (
    <div className="embla  relative rounded mx-2 md:mx-0">
      <div className={`embla__viewport w-full rounded ${carouselViewPortClass}`} ref={emblaRef}>
        <motion.div variants={carouselContainer} initial="hidden" animate="show" className={carouselContainerClass}>
          {children}
        </motion.div>
      </div>
      {navButtonsEnabled && NavButtons()}
      {navDotsEnabled && (
        <NavDots className="absolute bottom-0 flex items-center space-x-3">
          {scrollSnaps.map((_, index) => (
            <DotButton className="focus:outline-none" key={index} selected={index === selectedIndex} onClick={() => scrollTo(index)} />
          ))}
        </NavDots>
      )}
    </div>
  );
};

const CarouselNavButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.5rem;
  border-radius: 100%;
  color: black;
`;

const LeftNavButton = styled(CarouselNavButton)`
  position: absolute;
  top: 50%;
  left: 0;
  transform: translate(-50%, -50%);
`;
const RightNavButton = styled(CarouselNavButton)`
  position: absolute;
  top: 50%;
  right: 0;
  transform: translate(50%, -50%);
`;

const DotButton = styled.button`
  cursor: pointer;
  width: 12px;
  height: 12px;
  border-radius: 100%;
  background: ${({ selected }) => (selected ? '#FFFDFD' : '#73b94b')};
`;

const NavDots = styled.div`
  bottom: 16px;
  left: 50%;
  transform: translateX(-50%);
`;

const CarouselContainer = styled.div`
  height: 400px;
`;

export default Carousel;

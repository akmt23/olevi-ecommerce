import React from 'react';
import Tappable from '../Tappable';
import Link from 'next/link';

const ActionLink = ({ href, title }) => {
  return (
    <Link href={href ?? '/'}>
      <Tappable>
        <div className="flex items-center">
          <span className="mr-2 font-semibold text-sm lg:text-base">{title}</span>
          <div className="p-1 rounded-full bg-light-green text-black">
            <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M9 5l7 7-7 7" />
            </svg>
          </div>
        </div>
      </Tappable>
    </Link>
  );
};

export default ActionLink;

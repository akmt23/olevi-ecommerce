import { useAnimation, motion } from 'framer-motion';
import React, { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';

const InViewPopIn = ({ children, ...props }) => {
  const controls = useAnimation();
  const { ref, inView } = useInView();
  const boxVariants = {
    hidden: { opacity: 0, y: 100, type: 'spring' },
    visible: {
      opacity: 1,
      y: 0,
      type: 'spring',
      transition: {
        duration: 0.3,
        delay: 0.2,
      },
    },
  };

  useEffect(() => {
    if (inView) {
      controls.start('visible');
    }
  }, [controls, inView]);

  return (
    <motion.div ref={ref} variants={boxVariants} initial="hidden" animate={controls} {...props}>
      {children}
    </motion.div>
  );
};

export default InViewPopIn;

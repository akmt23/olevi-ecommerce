import React, { useEffect, useLayoutEffect, useState } from 'react';

const thumbsize = 14;

const PriceRangeSlider = ({ min, max, onChange, isFilterReseted }) => {
  const avg = (min + max) / 2;
  const [minVal, setMinVal] = useState(min);
  const [maxVal, setMaxVal] = useState(max - 1);

  useEffect(() => {
    setMinVal(min);
    setMaxVal(max - 1);
  }, [isFilterReseted]);

  const width = 220;
  const minWidth = thumbsize + ((avg - min) / (max - min)) * (width - 2 * thumbsize);
  const minPercent = ((minVal - min) / (avg - min)) * 100;
  const maxPercent = ((maxVal - avg) / (max - avg)) * 100;
  const styles = {
    min: {
      width: minWidth,
      left: 0,
      '--minRangePercent': `${minPercent}%`,
    },
    max: {
      width: thumbsize + ((max - avg) / (max - min)) * (width - 2 * thumbsize),
      left: minWidth,
      '--maxRangePercent': `${maxPercent}%`,
    },
  };

  return (
    <div className="space-y-5">
      <div className="grid grid-cols-3 bg-white p-2 rounded">
        <p className="text-sm col-start-1 col-end-2 justify-self-start">{parseInt(minVal)} ТГ</p>
        <p className="text-sm col-start-2 col-end-3 justify-self-center">—</p>
        <p className="text-sm col-start-3 col-end-4 justify-self-end">{parseInt(maxVal)} ТГ</p>
      </div>
      <div className="min-max-slider" data-legendnum="2" data-rangemin={min} data-rangemax={max} data-thumbsize={thumbsize} data-rangewidth={width}>
        <label htmlFor="min">Minimum price</label>
        <input
          id="min"
          className="min"
          style={styles.min}
          name="min"
          type="range"
          step="50"
          min={min}
          max={avg}
          value={minVal}
          onChange={({ target }) => {
            setMinVal(Number(target.value));
            onChange({ min: parseInt(minVal), max: parseInt(maxVal) });
          }}
        />
        <label htmlFor="max">Maximum price</label>
        <input
          id="max"
          className="max"
          style={styles.max}
          name="max"
          type="range"
          step="1"
          min={avg}
          max={max}
          value={maxVal}
          onChange={({ target }) => {
            setMaxVal(Number(target.value));
            onChange({ min: parseInt(minVal), max: parseInt(maxVal) });
          }}
        />
      </div>
    </div>
  );
};

export default PriceRangeSlider;

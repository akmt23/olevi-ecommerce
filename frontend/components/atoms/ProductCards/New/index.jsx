import React from 'react';
import { getStrapiMedia } from '../../../../utils/medias';
import BuyButtonSection from '@/organisms/BuyButtonSection';
import Image from 'next/image';
import Link from 'next/link';
import { useTranslations } from 'next-intl';

const NewProductCard = ({ id, description, title, price, category, imgUrl }) => {
  const t = useTranslations('AtomProducts');
  return (
    <div className="flex flex-col items-center p-4 border border-light-grey rounded-md">
      <div className="flex items-center justify-between w-full mb-4">
        <a className="uppercase text-center text-white text-xs px-6 bg-deep-yellow rounded" href="#">
          new
        </a>
        <button className="text-light-grey focus:outline-none">
          <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" viewBox="0 0 20 20" fill="currentColor">
            <path
              fill-rule="evenodd"
              d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
              clip-rule="evenodd"
            />
          </svg>
        </button>
      </div>
      <div className="mb-6">
        <Image height={192} width={250} className="h-48 object-contain" src={getStrapiMedia(imgUrl)} alt="product" />
      </div>
      <div className="flex flex-col mb-4 h-36">
        <span className="mb-2">
          <span className="bg-deep-blue px-4 text-2xl text-white rounded-md font-bold mr-1">{price}</span>
          <span className="font-semibold">тг</span>
        </span>
        <span className="text-left mb-2 text-lg font-semibold">{title}</span>
        <span className="text-xs uppercase text-light-grey font-semibold">{category}</span>
        <div className="flex justify-start w-full">
          <span className="bg-frozen-blue rounded-md px-2 mb-2">
            <Link className="frozen-blue capitalize" href={`/product/${id}`}>
              {t('action-look')}
            </Link>
          </span>
        </div>
      </div>
      <BuyButtonSection id={id} price={price} description={description} imgUrl={getStrapiMedia(imgUrl)} name={title}></BuyButtonSection>
    </div>
  );
};

export default NewProductCard;

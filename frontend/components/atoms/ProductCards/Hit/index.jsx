import React from 'react';
import Counter from '../../Counter';
import { getStrapiMedia } from '../../../../utils/medias';
import BuyButtonSection from '@/organisms/BuyButtonSection';
import { motion } from 'framer-motion';
import Image from 'next/image';
import Link from 'next/link';
import useDeviceDetect from '@/hooks/useDeviceDetect';
import { useTranslations } from 'next-intl';

const HitProductCard = ({ id, description, title, price, category, imgUrl, pathnames }) => {
  const { isMobile } = useDeviceDetect();

  const imageSizes = {
    mobile: { width: 150, height: 100 },
    desktop: { width: 250, height: 150 },
  };

  const t = useTranslations('AtomProducts');

  const imageSizeProps = imageSizes[isMobile ? 'mobile' : 'desktop'];

  return (
    <motion.div whileHover={{ y: -16 }} className="flex flex-col items-center p-2 lg:p-4 border border-light-grey rounded-md">
      <div className="flex items-center justify-between w-full mb-4">
        <a className="uppercase text-center text-white text-xs px-6 bg-deep-purple rounded" href="#">
          {t('hit')}
        </a>
        <button className="text-light-grey focus:outline-none">
          <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" viewBox="0 0 20 20" fill="currentColor">
            <path
              fill-rule="evenodd"
              d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
              clip-rule="evenodd"
            />
          </svg>
        </button>
      </div>
      <div className="mb-6">
        <Image {...imageSizeProps} className=" h-24 object-contain" src={getStrapiMedia(imgUrl)} alt="product" />
      </div>
      <div className="flex flex-col w-full items-start mb-2 h-36">
        <span>
          <span className="bg-deep-blue px-4 text-lg lg:text-2xl text-white rounded-md font-bold mr-1">{price}</span>
          <span className="font-semibold">тг</span>
        </span>
        <span className="text-left w-full text-sm lg:text-lg font-semibold my-2 truncate">{title}</span>
        {!isMobile && <span className="text-xs uppercase text-light-grey font-semibold">{category}</span>}
      </div>
      {!isMobile && (
        <div className="flex justify-start w-full">
          <span className="bg-frozen-blue rounded-md px-2 mb-2">
            <Link className="frozen-blue capitalize" href={`/product/${id}`}>
              {t('action-look')}
            </Link>
          </span>
        </div>
      )}
      <BuyButtonSection id={id} price={price} description={description} imgUrl={getStrapiMedia(imgUrl)} name={title}></BuyButtonSection>
    </motion.div>
  );
};

export default HitProductCard;

import React, { useMemo } from 'react';
import Link from 'next/link';
import useDeviceDetect from '@/hooks/useDeviceDetect';
import { getStrapiMedia } from 'utils/medias';
import BuyButtonSection from '@/organisms/BuyButtonSection';
import { motion } from 'framer-motion';
import Image from 'next/image';
import HeartButton from '@/atoms/HeartButton';
import { useTranslations } from 'next-intl';

const getProductLabelFromType = type => {
  const productLabels = {
    discount: props => (
      <Link href="/">
        <span className="uppercase text-center text-white text-xs px-6 bg-coral-red rounded font-semibold">-{props.discountPercent}%</span>
      </Link>
    ),
    hit: props => (
      <Link href="/">
        <span className="uppercase text-center text-white text-xs px-6 bg-deep-purple rounded">Хит</span>
      </Link>
    ),
    new: props => (
      <Link href="/">
        <span className="uppercase text-center text-white text-xs px-6 bg-deep-yellow rounded">new</span>
      </Link>
    ),
    base: props => <div></div>,
  };
  return productLabels[type] ?? null;
};

const getPriceLabel = isDiscount => {
  if (isDiscount) {
    return props => (
      <span className="mb-2 flex flex-wrap">
        <span className="bg-coral-red px-4 text-xl text-white rounded-md font-bold mr-1">
          {Math.ceil(props.price - (props.price * props.discountPercent) / 100)}
        </span>
        <span className="font-semibold mr-2 text-base">тг</span>
        <span className=" text-sm text-light-grey line-through">{props.initialPrice} тг</span>
      </span>
    );
  }
  return props => (
    <span>
      <span className="bg-deep-blue px-4 text-lg lg:text-2xl text-white rounded-md font-bold mr-1">{props.price}</span>
      <span className="font-semibold">тг</span>
    </span>
  );
};

const getProductData = product => {
  const imgUrl = getStrapiMedia(product?.image?.url ?? '');
  const category = product?.categories?.[0]?.name;
  const discountData = {
    initialPrice: product?.price,
    discountPercent: 9,
  };
  return { imgUrl, category, discountData, ...product };
};

const getImageSizes = isMobile => {
  const imageSizes = {
    mobile: { width: 150, height: 100 },
    desktop: { width: 250, height: 150 },
  };
  return imageSizes[isMobile ? 'mobile' : 'desktop'];
};

const ProductCard = ({ type = 'base', product }) => {
  const { isMobile } = useDeviceDetect();
  const productData = useMemo(() => getProductData(product), [product]);
  const t = useTranslations('AtomProducts');
  if (!product || !product.status === 'published' || !productData.title) {
    return null;
  }
  const { id, description, title, price, imgUrl, category, discountData } = productData || {};
  const ProductLabel = getProductLabelFromType(type) ?? null;
  const PriceLabel = getPriceLabel(type === 'discount');
  const imageSizeProps = getImageSizes(isMobile);

  return (
    <div style={{ maxHeight: 450 }} className="flex flex-col items-center p-2 lg:p-4 border border-light-grey rounded-md">
      <div className="flex items-center justify-between w-full mb-4">
        <ProductLabel {...discountData}></ProductLabel>
        <HeartButton product={product} />
      </div>
      <div className="mb-6">
        <Image {...imageSizeProps} className=" h-24 object-contain" src={imgUrl} alt="product" />
      </div>
      <div className="flex flex-col w-full items-start mb-2 h-36">
        <PriceLabel price={price} {...discountData}></PriceLabel>
        <span className="text-left w-full text-sm lg:text-lg font-semibold my-2 truncate">{title}</span>
        {!isMobile && <span className="text-xs uppercase text-light-grey font-semibold h-4">{category}</span>}
      </div>

      <div className="flex justify-start w-full">
        <span className="bg-frozen-blue rounded-md px-2 mb-2">
          <Link className="frozen-blue capitalize" href={`/product/${id}`}>
            {t('action-look')}
          </Link>
        </span>
      </div>
      <BuyButtonSection product={product}></BuyButtonSection>
    </div>
  );
};

export default ProductCard;

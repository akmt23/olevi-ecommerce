import React from 'react';
import { getStrapiMedia } from '../../../../utils/medias';
import Counter from '../../Counter';
import BuyButtonSection from '@/organisms/BuyButtonSection';
import Image from 'next/image';
import Link from 'next/link';
import { useTranslations } from 'next-intl';

const DiscountProductCard = ({ id, description, title, price, category, imgUrl, discountPercent, initialPrice }) => {
  const t = useTranslations('AtomProducts');

  return (
    <div className=" flex flex-col items-center p-4 border border-light-grey rounded-md">
      <div className="mb-6">
        <Image height={192} width={250} className="h-48 object-contain" src={getStrapiMedia(imgUrl)} alt="product" />
      </div>
      <div className="flex flex-col mb-4 h-36">
        <span className="mb-2">
          <span className="bg-coral-red px-4 text-2xl text-white rounded-md font-bold mr-1">{Math.ceil(price - (price * discountPercent) / 100)}</span>
          <span className="font-semibold mr-2">тг</span>
          <span className=" text-lg text-light-grey line-through">{initialPrice} тг</span>
        </span>
        <span className="text-left mb-2 text-lg font-semibold">{title}</span>
        <span className="text-xs uppercase text-light-grey font-semibold">{category}</span>
        <div className="flex justify-start w-full">
          <span className="bg-frozen-blue rounded-md px-2 mb-2">
            <Link className="frozen-blue capitalize" href={`/product/${id}`}>
              {t('action-look')}
            </Link>
          </span>
        </div>
      </div>
      <BuyButtonSection id={id} price={price} description={description} imgUrl={getStrapiMedia(imgUrl)} name={title} />
    </div>
  );
};

export default DiscountProductCard;

import React, { useState } from 'react';
import Tappable from '../Tappable';
import axios from 'axios';
import StarRatingComponent from 'react-star-rating-component';

const ReviewForm = ({ product, close }) => {
  const [review, setReview] = useState({
    email: '',
    name: '',
    rate: 1,
    text: '',
  });
  const fillReview = e => {
    const { name, value } = e.target;
    setReview(prevState => ({
      ...prevState,
      [name]: value,
    }));
  };
  const starRate = value => {
    setReview(prevState => ({
      ...prevState,
      rate: value,
    }));
  };
  const handleSubmit = e => {
    axios.post('http://104.248.139.194:1337/feedbacks', { ...review, product }).then(res => {
      setReview(prevState => ({ ...prevState, email: '', name: '', rate: 1, text: '' }));
    });
    close = false;
  };
  return (
    <div className="container flex flex-col shadow-md lg:w-3/5 rounded-lg bg-frozen-blue border-light-grey-500 py-4 space-y-3">
      <div className="flex flex-col lg:flex-row lg:space-x-8">
        <div className="flex flex-col flex-1 lg:mx-2 space-y-3">
          <div className="flex flex-col space-y-2">
            <label className="font-light text-dark-grey">Email Адрес</label>
            <input
              className="shadow-sm appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              name="email"
              value={review.email}
              onChange={fillReview}
            />
          </div>
          <div className="flex flex-col space-y-2">
            <label className="font-light text-dark-grey">Имя</label>
            <input
              className="shadow-sm appearance-none border rounded w-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
              name="name"
              value={review.name}
              onChange={fillReview}
            />
          </div>
          <div className="flex flex-col space-y-2">
            <label className="font-light text-dark-grey">Оценка</label>
            <div className="">
              <StarRatingComponent name="rate" editing={true} starCount={5} value={review.rate} onStarClick={starRate} />
            </div>
          </div>
        </div>
        <div className="flex flex-col flex-1 space-y-2">
          <label className="font-light text-dark-grey">Доп. Инфо</label>
          <textarea
            className="shadow-sm appearance-none border rounded w-full h-full py-2 px-3 leading-tight focus:outline-none focus:shadow-outline"
            name="text"
            placeholder=""
            value={review.text}
            onChange={fillReview}
          />
        </div>
      </div>
      <div className="flex flex-col">
        <Tappable className="bg-light-green rounded-md py-2 font-bold text-lg" onClick={handleSubmit}>
          Отправить
        </Tappable>
      </div>
    </div>
  );
};

export default ReviewForm;

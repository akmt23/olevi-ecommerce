import React from 'react';
import { motion, AnimatePresence } from 'framer-motion';

const Collapsible = ({ title, children }) => {
  return (
    <div className="flex flex-col mb-2">
      <button className="flex items-center justify-start w-full focus:outline-none">
        <motion.div className="mr-2" animate={{ rotate: '90deg' }} transition={{ duration: 0.2, ease: 'easeIn' }}>
          <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={3} d="M9 5l7 7-7 7" />
          </svg>
        </motion.div>
        <span className="font-normal uppercase text-dark-grey mt-2 text-sm">{title}</span>
      </button>
      <div className="flex flex-col mt-2 border rounded-sm">
        <AnimatePresence initial={false}>
          <section
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: {
                opacity: 1,
                height: 'auto',
                scale: [1, 1.1, 1],
                position: 'static',
              },
              collapsed: {
                opacity: 0,
                height: 0,
              },
            }}
            transition={{ duration: 0.5, type: 'spring' }}>
            {children}
          </section>
        </AnimatePresence>
      </div>
    </div>
  );
};

export default Collapsible;

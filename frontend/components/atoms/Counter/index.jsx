import React, { useState } from 'react';
import Tappable from '../Tappable';

const Conter = ({ initial = 1, onChange = () => {} }) => {
  const [count, setCount] = useState(initial);
  const decrement = () => {
    setCount(actualCount => {
      if (actualCount - 1 < 1) {
        onChange(actualCount);
        return actualCount;
      }
      onChange(actualCount - 1);
      return actualCount - 1;
    });
  };

  const increment = () => {
    setCount(actualCount => {
      onChange(actualCount + 1);
      return actualCount + 1;
    });
  };
  return (
    <div className="flex items-center px-2 py-1 border border-light-grey rounded-md justify-evenly space-x-1">
      <Tappable className="text-dark-grey focus:outline-none" onClick={decrement}>
        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M18 12H6" />
        </svg>
      </Tappable>
      <span className="text-center">{count}</span>
      <Tappable className=" text-dark-grey focus:outline-none" onClick={increment}>
        <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
        </svg>
      </Tappable>
    </div>
  );
};

export default Conter;

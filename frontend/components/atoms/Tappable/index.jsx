import React from 'react';
import { motion } from 'framer-motion';

const Tappable = props => {
  return <motion.button {...props} whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} />;
};

export default Tappable;

import { AnimatePresence, motion } from 'framer-motion';
import React from 'react';
import Select from 'react-dropdown-select';
import styled from 'styled-components';

const CustomSelect = ({ onChange, options, placeholder, ...props }) => {
  const dropdownRenderer = ({ props, state, methods }) => {
    const dropdownOptions = props.options;
    return (
      <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} className="flex items-start flex-col p-2 rounded-md bg-white ">
        {dropdownOptions.map(el => (
          <button
            onClick={() => methods.addItem(el)}
            className="font-semibold mb-1 hover:bg-deep-blue transition-all ease-in-out duration-200 hover:text-white w-full rounded-md text-left p-2">
            {el[props.labelField]}
          </button>
        ))}
      </motion.div>
    );
  };
  return (
    <StyledSelect
      className="bg-white border-none rounded-md px-4 font-sans font-semibold text-sm"
      placeholder={placeholder}
      options={options}
      values={[options[0]]}
      clearOnSelect
      {...props}
      dropdownRenderer={dropdownRenderer}
      onChange={values => onChange(values)}
    />
  );
};

const StyledSelect = styled(Select)`
  background-color: white;
  border-radius: 0.375rem;
  border: none;
  padding: 0 1rem;
  color: black;
  box-shadow: none;
`;

const ItemLabel = ({ item, methods, props }) => {
  return (
    <div key={item.name}>
      <button onClick={() => methods.addItem(item)}>{item.name}</button>
    </div>
  );
};
export default CustomSelect;

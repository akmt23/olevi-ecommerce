import React, { useState } from 'react';
import AppFooter from '@/templates/Footer';
import AppHeader from '@/templates/Header';
import Sidebar from './organisms/Sidebar';
import { useRecoilState } from 'recoil';
import { sidebarState } from 'data/atoms/sidebarAtom';
import { searchState } from 'data/atoms/searchAtom';
import Search from './organisms/Search';
import Router from 'next/router';

const Base = ({ children, categories }) => {
  const [sidebar, setSidebar] = useRecoilState(sidebarState);
  const [search, setSearch] = useRecoilState(searchState);
  Router.events.on('routeChangeStart', () => {
    setSearch(false);
    setSidebar(false);
  });

  return (
    <div className="bg-white-custom">
      <AppHeader categories={categories}></AppHeader>
      <div className="flex flex-col min-h-screen w-full">
        <div className="flex-grow">{children}</div>
      </div>
      <AppFooter></AppFooter>
      <Sidebar visible={sidebar} onChange={sidebar => setSidebar(sidebar)}></Sidebar>
      <Search visible={search} onChange={search => setSearch(search)}></Search>
    </div>
  );
};

export default Base;
